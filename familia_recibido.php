<?php
include("includes/header.php");
//session_destroy();
$id = isset($_GET['id'])?$_GET['id']:0;
?>

<link rel="stylesheet" type="text/css" href="css/producto_style.css">
<script src="js/producto.js"></script>

<div class="wrapper">
	<div id="contenedor" class="form_box1">
		<div class="card-body">
			<div>
			    <!--form action="donacion_ent_consulta.php" method="POST"-->
			       	
					      <h3><strong>Donaciones</strong></h3>  
					      <?php
					        $sql = "SELECT afad.Don_Donacion, afad.Don_FechaEntrega FROM afa_donacion afad
									INNER JOIN afa_familia afaf
									ON afaf.Fam_Familia = afad.Fam_Familia
									WHERE afaf.Fam_Familia = {$id} AND afad.Don_Estado='Entregada';";
					        $resultset = mysqli_query($con, $sql) or die("database error:". mysqli_error($con));
					        while( $record = mysqli_fetch_assoc($resultset) ) {
					      ?>
					            <div class="card" style="width: 25rem;">
							        <div class="card-body">
							            <dl class="row">
							                <dt class="col-sm-7">
							                    <label>ID Donación</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Don_Donacion']; ?>
							                </dd>
							                <dt class="col-sm-7">
							                    <label>Fecha que recibió</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Don_FechaEntrega']; ?>
							                </dd>							             					               
							            </dl>


							            <div class="container_donacion">
							            	<h5>Detalle de donación</h5>
											<?php 
												$query_run = mysqli_query($con, "SELECT afad.Don_FechaEntrega, afap.Pro_Nombre, afadd.DDR_Cantidad FROM afa_donacion afad
													INNER JOIN afa_familia afaf
													ON afaf.Fam_Familia = afad.Fam_Familia
													INNER JOIN afa_detalle_donacionrecibida afadd
													ON afadd.Don_Donacion = afad.Don_Donacion
													INNER JOIN afa_producto afap
													ON afap.Pro_Producto = afadd.DDR_Producto
													WHERE afaf.Fam_Familia = {$id};");
											?>
											<table class="table table-hover">
											  <thead>
											    <tr>
											      <th scope="col">Fecha entrega</th>
											      <th scope="col">Producto</th>
											      <th scope="col">Cantidad</th>
											    </tr>
											  </thead>
											  <?php
												if ($query_run) {
													foreach ($query_run as $row){
											?>
											  <tbody>
											    <tr>
											      	<td> <?php echo $row['Don_FechaEntrega'];?></td>
												    <td> <?php echo $row['Pro_Nombre'];?> </td>
												    <td class="text-center"> <?php echo $row['DDR_Cantidad'];?> </td>
											    </tr>
											  </tbody>
											<?php
													}
												}else{
													echo "No se encontraron registros";
												}
											?>
											</table>
										</div>


							            <form style="text-align: center;">
							                <a href="donacion_entregada.php" class="btn btn-outline-dark">Volver <a/> 
							            </form>
							        </div>
							    </div>

					      <?php } ?>			
			  	</form>
			</div>
		</div>
	</div>
</div>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>
<?php 

	include("includes/main_header.php");

?>

  <div class="header">
    <div class="header-content">
      <div class="containerFix">
        <h1 data-w-id="b777ef2d-ac03-cea3-ccc5-52beeee5222a" style="-webkit-transform:translate3d(0, 40PX, 0) scale3d(1, 1, 1) rotateX(-50DEG) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 40PX, 0) scale3d(1, 1, 1) rotateX(-50DEG) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 40PX, 0) scale3d(1, 1, 1) rotateX(-50DEG) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 40PX, 0) scale3d(1, 1, 1) rotateX(-50DEG) rotateY(0) rotateZ(0) skew(0, 0);transform-style:preserve-3d;opacity:0" class="h1">¡Bienvenido!</h1>
        <p data-w-id="cd085e48-08fc-46a5-a6c3-97346f451e6d" style="opacity:0;-webkit-transform:translate3d(0, 60PX, 0) scale3d(1, 1, 1) rotateX(-60DEG) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 60PX, 0) scale3d(1, 1, 1) rotateX(-60DEG) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 60PX, 0) scale3d(1, 1, 1) rotateX(-60DEG) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 60PX, 0) scale3d(1, 1, 1) rotateX(-60DEG) rotateY(0) rotateZ(0) skew(0, 0);transform-style:preserve-3d; letter-spacing: 1px;" class="paragraph">Estamos felices de tenerte aquí. All for All es una plataforma que permite a nuestros suscriptores ser parte de una comunidad cuyo objetivo es el de ayudar a personas en necesidad a través de donaciones y participaciones en actividades en las que se requiere voluntariado.</p>
      </div>
    </div>
  </div>
  <div class="container-2 w-container">
    <div class="w-row">
      <div class="column-3 w-col w-col-6">
        <h2 class="heading"><a href="about.php" class="link-2">Nuestra visión y misión</a></h2>
        <p class="paragraph-7">Conoce más sobre quiénes somos, qué hacemos y para qué lo hacemos.<br>‍</p>
      </div>
      <div class="column-4 w-col w-col-6">
        <h2 class="heading-2"><a href="instituciones_info.php" class="link-3">Conocer Instituciones</a></h2>
        <p class="paragraph-6">Encontrarás las instituciones a las que se les provee de una plataforma para que personas como tú puedan conocerlas y apoyarlas. </p>
      </div>
    </div>
  </div>
  <!--div class="separator cc-background-grey">
    <div class="container"></div>
  </div-->
  <header id="hero" class="hero">
    <div class="flex-container-4 w-container">
      <div class="div-block-3">
        <h1 class="heading-3">Únete</h1>
        <p class="paragraph-8">Podrás conocer distintas organizaciones y empezar a ayudar a quienes más lo necesitan.</p><a href="suscripcion.php" class="button-4 w-button">Suscribirse</a></div>
      <div class="hero-image-mask"><img src="images/63293-min.jpg" srcset="images/63293-min-p-500.jpeg 500w, images/63293-min-p-1080.jpeg 1080w, images/63293-min-p-1600.jpeg 1600w, images/63293-min-p-2000.jpeg 2000w, images/63293-min-p-2600.jpeg 2600w, images/63293-min-p-3200.jpeg 3200w, images/63293-min.jpg 7000w" sizes="(max-width: 479px) 92vw, (max-width: 767px) 95vw, (max-width: 991px) 316.1875px, 471.03125px" alt="" class="hero-image"></div>
    </div>
  </header>
  <!--div class="separator cc-background-grey">
    <div class="container"></div>
  </div-->
  <div class="footer">
    <div class="container cc-footer">
      <div class="footer-column cc-footer"><a href="index.php" aria-current="page" class="navigation-logo w-inline-block w--current"><img src="images/all-x-all-2.png" width="143" srcset="images/all-x-all-2-p-500.png 500w, images/all-x-all-2-p-800.png 800w, images/all-x-all-2.png 1030w" sizes="(max-width: 479px) 30vw, 143px" alt=""></a>
        <div class="text-footer-credits">© 2020 All for All Inc, All rights reserved.</div>
      </div>
      <div class="footer-column" style="max-height: 50px;">
        <div class="footer-links-list"><a href="index.php" aria-current="page" class="link-footer w--current">Inicio</a><a href="about.php" class="link-footer">Conócenos</a><a href="instituciones_info.php" class="link-footer">Instituciones</a><a href="intermediario_info.php" class="link-footer">Intermediarios</a></div>
        <div class="footer-social"><a href="#" class="link-social w-inline-block"><img src="images/icon-facebook.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-twitter.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-instagram.svg" alt=""></a></div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5f571a518e3cdfe11baf4271" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>
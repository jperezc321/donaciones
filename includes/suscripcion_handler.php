<?php
//declaración de variables
$susnombre = "";
$susapellido = "";
$susemail = "";
$tiposuscripcion = "";
$susdate = "";
$error_array = array(); //guarda los mensajes de error

if (isset($_POST['sus_button'])) {
	//registro de los valores del form

	//Nombre
	$susnombre = strip_tags($_POST['sus_nombre']);//quita los strip tags que pueden ingresar
	$susnombre = str_replace(' ', '', $susnombre);//quita los espacios
	$susnombre = ucfirst(strtolower($susnombre));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['sus_nombre'] = $susnombre; //guarda el nombre dentro de la variable de sesión

	//apellido
	$susapellido = strip_tags($_POST['sus_apellido']);//quita los strip tags que pueden ingresar
	$susapellido = str_replace(' ', '', $susapellido);//quita los espacios
	$susapellido = ucfirst(strtolower($susapellido));//convierte las mayusculas en minusculas solo dejando la primera como susapellido
	$_SESSION['sus_apellido'] = $susapellido;

	//email1
	$susemail = strip_tags($_POST['sus_email']);//quita los strip tags que pueden ingresar
	$susemail = str_replace(' ', '', $susemail);//quita los espacios
	//$regemail = ucfirst(strtolower($regemail));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['sus_email'] = $susemail;

	//email2
	$tiposuscripcion = $_POST['tipo_suscripcion'];
	$_SESSION['tipo_suscripcion'] = $tiposuscripcion;

	//password
	//$regpass = strip_tags($_POST['reg_password']);//quita los strip tags que pueden ingresar

	//date
	$susdate = date("Y-m-d");//current date

	//if ($regemail == $regemail2) {
		//si el memail tiene un formato válido
		if (filter_var($susemail, FILTER_VALIDATE_EMAIL)) {

			$susemail = filter_var($susemail, FILTER_VALIDATE_EMAIL);

			//verificar si el email existe
			$e_check1 = mysqli_query($con, "SELECT Sus_Email FROM afa_suscriptor WHERE Sus_Email='$susemail'");
			//contar el numero de filas devueltas
			$num_filas1 = mysqli_num_rows($e_check1);

			if($num_filas1 > 0){
				array_push($error_array, "Esta dirección de correo electrónico ya está registrada<br>");
			}
		}else{
			array_push($error_array, "Formato inválido<br>");
		}
	//}else{
	//	array_push($error_array, "Los correo electrónicos no coinciden<br>");
	//}

	//nombre debe de tener más de 2 caracteres
	if (strlen($susnombre) > 25 || strlen($susnombre) < 2) {
		array_push($error_array,"El nombre deberia de tener entre 2 y 25 caracteres<br>");		
	}

	//apellido debe de tener más de 2 caracteres
	if (strlen($susapellido) > 25 || strlen($susapellido) < 2) {
		array_push($error_array,"El apellido deberia de tener entre 2 y 25 caracteres<br>");		
	}
	
	//if ($regpass != $regpass2) {
		//array_push($error_array, "Las contraseñas no coinciden<br>");
	//}
	//else {
		//if (preg_match('/[^A-Za-z0-9]/', $regpass)) {
		//	array_push($error_array, "La clave debe contener al menos 1 letra mayúscula, una letra minúsculo y un carácter numérico<br>");
		//}
	//}

	//if(strlen($regpass > 30 || strlen($regpass) < 5)) {
	//	array_push($error_array, "La contraseña debe de tener entre 5 y 30 carácteres<br>");
	//}
	
	if (empty($error_array)) {
		//$regpass = md5($regpass); //encriptación de passwd
	
		$query = mysqli_query($con, "INSERT INTO afa_suscriptor VALUES ('', '$susnombre', '$susapellido', '$susemail', '$tiposuscripcion', '$susdate', 'Y')");
		//echo "Error: " . mysqli_error($con);

		array_push($error_array, "<span style='color: #14C800;'>Listo, ya puedes ir al login</span><br>");

		//limpiar las variables de sesión
		$_SESSION['sus_nombre'] = "";
		$_SESSION['sus_apellido'] = "";
		$_SESSION['sus_email'] = "";
		$_SESSION['tipo_suscripcion'] = "";
	}


}

?>

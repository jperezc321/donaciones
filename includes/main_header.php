<?php
//require 'config/config.php';

?>

<!DOCTYPE html>
<html data-wf-page="5f571a5117ef9d70403a8737" data-wf-site="5f571a518e3cdfe11baf4271">
<head>
  <meta charset="UTF-8" />
  <title>Home</title>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/proyectotesis.css" rel="stylesheet" type="text/css">

  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Exo:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Libre Franklin:200,300,regular,600,800,900","IBM Plex Mono:regular"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <!-- <link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon"> -->
  <link href="images/webclip.png" rel="apple-touch-icon">
  <style>
  body {
  -moz-font-feature-settings: "liga" on;
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  font-feature-settings: "liga" on;
  text-rendering: optimizeLegibility;
}
</style>
</head>
<body class="body">
  <div class="navigation"></div>
  <div data-collapse="medium" data-animation="default" data-duration="400" data-easing="ease-out" data-easing2="ease-out" role="banner" class="navigation w-nav">
    <div class="navigation-container">
        <a href="index.php" aria-current="page" class="logo w-inline-block w--current">
          <img src="images/all-x-all-2.png" loading="lazy" width="164" srcset="images/all-x-all-2-p-500.png 500w, images/all-x-all-2-p-800.png 800w, images/all-x-all-2.png 1030w" sizes="(max-width: 479px) 55vw, 164px" alt="">
        </a>
      <nav role="navigation" class="nav-menu w-nav-menu">
        <a href="index.php" aria-current="page" class="nav-link w-nav-link w--current">Inicio</a>
        <a href="about.php" class="nav-link w-nav-link">Conócenos</a>
        <a href="instituciones_info.php" class="nav-link w-nav-link">Instituciones</a>
        <a href="intermediario_info.php" class="nav-link w-nav-link">Intermediarios</a>
        <div class="bullet"></div>
        <a href="suscripcion.php" class="navigation-button w-button">Suscribirse</a>
      </nav>
      <div class="menu-button w-nav-button">
        <div class="icon-2 w-icon-nav-menu"></div>
      </div>
    </div>
  </div>
<?php
//declaración de variables
$pronombre = "";
$protipo = "";
$prodate = "";
$protalla = "";
$progenero = "";
$proedad = "";
$error_array = array(); //guarda los mensajes de error

if (isset($_POST['pro_button'])) {
	//registro de los valores del form

	//Nombre
	$pronombre = strip_tags($_POST['pro_nombre']);//quita los strip tags que pueden ingresar
	$pronombre = ucfirst(strtolower($pronombre));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['pro_nombre'] = $pronombre; //guarda el nombre dentro de la variable de sesión

	//tipo de producto
	$protipo = $_POST['pro_tipo'];
	$_SESSION['pro_tipo'] = $protipo;

	//date
	$prodate = date("Y-m-d");//current date

	//talla 
	$protalla = $_POST['pro_talla'];
	$_SESSION['pro_talla'] = $protalla;
	
	//género
	$progenero = $_POST['pro_genero'];
	$_SESSION['pro_genero'] = $progenero;

	//edad
	$proedad = $_POST['pro_edad'];
	$_SESSION['pro_edad'] = $proedad;	

	if (empty($protalla) && empty($progenero) && empty($proedad)) {
		$query = mysqli_query($con, "INSERT INTO AFA_Producto VALUES ('', '$pronombre', '$protipo', 'Y', '$prodate', null, null, null)");
	}else{
		$query = mysqli_query($con, "INSERT INTO AFA_Producto VALUES ('', '$pronombre', '$protipo', 'Y', '$prodate', '$protalla', '$progenero', '$proedad')");
	}

	if ($query) {
		echo '<script> alert("Datos insertados"); </script>';
		header("Location:producto.php");
	}else{
		echo '<script> alert("Los datos no se pudieron actualizar"); </script>';
	}

		//$query = mysqli_query($con, "INSERT INTO AFA_Producto VALUES ('', '$pronombre', '0', '$protipo', 'Y', '$prodate')");
		//echo "Error: " . mysqli_error($con);

		array_push($error_array, "<span style='color: #14C800;'>Listo, ya puedes ir al login</span><br>");

		//limpiar las variables de sesión
		$_SESSION['pro_nombre'] = "";
		$_SESSION['pro_tipo'] = "";
		$_SESSION['pro_talla'] = "";
		$_SESSION['pro_genero'] = "";
		$_SESSION['pro_edad'] = "";


}

?>
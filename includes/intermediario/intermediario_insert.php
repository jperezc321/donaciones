<?php
//declaración de variables
$intnombre = "";
$intemail = "";
$intdireccion = "";
$inttelefono = "";
$insdescripcion = "";
$intdate = "";
$error_array = array(); //guarda los mensajes de error

if (isset($_POST['int_button'])) {
	//registro de los valores del form

	//Nombre
	$intnombre = strip_tags($_POST['int_nombre']);//quita los strip tags que pueden ingresar
	//$intnombre = str_replace(' ', '', $intnombre);//quita los espacios
	$intnombre = ucfirst(strtolower($intnombre));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['int_nombre'] = $intnombre; //guarda el nombre dentro de la variable de sesión

	//email
	$intemail = strip_tags($_POST['int_email']);//quita los strip tags que pueden ingresar
	$intemail = str_replace(' ', '', $intemail);//quita los espacios
	//$regemail = ucfirst(strtolower($regemail));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['int_email'] = $intemail;

	//direccion
	$intdireccion = strip_tags($_POST['int_direccion']);//quita los strip tags que pueden ingresar
	//$intdireccion = str_replace(' ', '', $intdireccion);//quita los espacios
	//$intdireccion = ucfirst(strtolower($intdireccion));//convierte las mayusculas en minusculas solo dejando la primera como susapellido
	$_SESSION['int_direccion'] = $intdireccion;

	//telefono
	$inttelefono = strip_tags($_POST['int_telefono']);//quita los strip tags que pueden ingresar
	$inttelefono = str_replace(' ', '', $inttelefono);//quita los espacios
	//$inttelefono = ucfirst(strtolower($inttelefono));//convierte las mayusculas en minusculas solo dejando la primera como susapellido
	$_SESSION['int_telefono'] = $inttelefono;

	$intdescripcion = strip_tags($_POST['int_descripcion']);//quita los strip tags que pueden ingresar
	//$instelefono = str_replace(' ', '', $instelefono);//quita los espacios
	$intdescripcion = ucfirst(strtolower($intdescripcion));//convierte las mayusculas en minusculas solo dejando la primera como susapellido
	$_SESSION['int_descripcion'] = $intdescripcion;

	//date
	$intdate = date("Y-m-d");//current date

	//if ($regemail == $regemail2) {
		//si el memail tiene un formato válido
		if (filter_var($intemail, FILTER_VALIDATE_EMAIL)) {

			$intemail = filter_var($intemail, FILTER_VALIDATE_EMAIL);

			//verificar si el email existe
			$e_check = mysqli_query($con, "SELECT Int_Email FROM AFA_Intermediario WHERE Int_Email='$intemail'");
			//contar el numero de filas devueltas
			$num_filas = mysqli_num_rows($e_check);

			if($num_filas > 0){
				array_push($error_array, "Esta dirección de correo electrónico ya está registrada<br>");
			}
		}else{
			array_push($error_array, "Formato inválido<br>");
		}
	//}else{
	//	array_push($error_array, "Los correo electrónicos no coinciden<br>");
	//}

	//nombre debe de tener más de 2 caracteres
	//if (strlen($intnombre) > 50 || strlen($intnombre) < 5) {
	//	array_push($error_array,"El nombre deberia de tener entre 5 y 50 caracteres<br>");		
	//}

	//apellido debe de tener más de 2 caracteres
	//if (strlen($susapellido1) > 25 || strlen($susapellido1) < 2) {
	//	array_push($error_array,"El apellido deberia de tener entre 2 y 25 caracteres<br>");		
	//}
	
	
	if (empty($error_array)) {
	
		$query = mysqli_query($con, "INSERT INTO AFA_Intermediario VALUES ('', '$intnombre', '$intemail', '$intdireccion', '$inttelefono', '$intdate', 'Y', null, '$intdescripcion')");
		//echo "Error: " . mysqli_error($con);

		array_push($error_array, "<span style='color: #14C800;'>Listo, ya puedes ir al login</span><br>");

		//limpiar las variables de sesión
		$_SESSION['int_nombre'] = "";
		$_SESSION['int_email'] = "";
		$_SESSION['int_direccion'] = "";
		$_SESSION['int_telefono'] = "";
	}


}

?>
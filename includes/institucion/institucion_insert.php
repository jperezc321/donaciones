<?php
//declaración de variables
$insnombre = "";
$insemail = "";
$insdireccion = "";
$instelefono = "";
$insdescripcion = "";
$insdate = "";
$error_array = array(); //guarda los mensajes de error

if (isset($_POST['ins_button'])) {
	//registro de los valores del form

	//Nombre
	$insnombre = strip_tags($_POST['ins_nombre']);//quita los strip tags que pueden ingresar
	//$intnombre = str_replace(' ', '', $intnombre);//quita los espacios
	$insnombre = ucfirst(strtolower($insnombre));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['ins_nombre'] = $insnombre; //guarda el nombre dentro de la variable de sesión

	//email
	$insemail = strip_tags($_POST['ins_email']);//quita los strip tags que pueden ingresar
	$insemail = str_replace(' ', '', $insemail);//quita los espacios
	//$regemail = ucfirst(strtolower($regemail));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['ins_email'] = $insemail;

	//direccion
	$insdireccion = strip_tags($_POST['ins_direccion']);//quita los strip tags que pueden ingresar
	//$intdireccion = str_replace(' ', '', $intdireccion);//quita los espacios
	//$intdireccion = ucfirst(strtolower($intdireccion));//convierte las mayusculas en minusculas solo dejando la primera como susapellido
	$_SESSION['ins_direccion'] = $insdireccion;

	//telefono
	$instelefono = strip_tags($_POST['ins_telefono']);//quita los strip tags que pueden ingresar
	$instelefono = str_replace(' ', '', $instelefono);//quita los espacios
	//$inttelefono = ucfirst(strtolower($inttelefono));//convierte las mayusculas en minusculas solo dejando la primera como susapellido
	$_SESSION['ins_telefono'] = $instelefono;

	$insdescripcion = strip_tags($_POST['ins_descripcion']);//quita los strip tags que pueden ingresar
	//$instelefono = str_replace(' ', '', $instelefono);//quita los espacios
	$insdescripcion = ucfirst(strtolower($insdescripcion));//convierte las mayusculas en minusculas solo dejando la primera como susapellido
	$_SESSION['ins_descripcion'] = $insdescripcion;

	//date
	$insdate = date("Y-m-d");//current date

	//if ($regemail == $regemail2) {
		//si el memail tiene un formato válido
		if (filter_var($insemail, FILTER_VALIDATE_EMAIL)) {

			$insemail = filter_var($insemail, FILTER_VALIDATE_EMAIL);

			//verificar si el email existe
			$e_check = mysqli_query($con, "SELECT Ins_Email FROM AFA_Institucion WHERE Ins_Email='$insemail'");
			//contar el numero de filas devueltas
			$num_filas = mysqli_num_rows($e_check);

			if($num_filas > 0){
				array_push($error_array, "Esta dirección de correo electrónico ya está registrada<br>");
			}
		}else{
			array_push($error_array, "Formato inválido<br>");
		}
	//}else{
	//	array_push($error_array, "Los correo electrónicos no coinciden<br>");
	//}

	//nombre debe de tener más de 2 caracteres
	//if (strlen($intnombre) > 50 || strlen($intnombre) < 5) {
	//	array_push($error_array,"El nombre deberia de tener entre 5 y 50 caracteres<br>");		
	//}

	//apellido debe de tener más de 2 caracteres
	//if (strlen($susapellido1) > 25 || strlen($susapellido1) < 2) {
	//	array_push($error_array,"El apellido deberia de tener entre 2 y 25 caracteres<br>");		
	//}
	
	
	if (empty($error_array)) {
	
		$query = mysqli_query($con, "INSERT INTO AFA_Institucion VALUES ('', '$insnombre', '$insemail', '$insdireccion', '$instelefono', '$insdate', '', '$insdescripcion', 'Y')");
		//echo "Error: " . mysqli_error($con);

		array_push($error_array, "<span style='color: #14C800;'>Listo, ya puedes ir al login</span><br>");

		//limpiar las variables de sesión
		$_SESSION['ins_nombre'] = "";
		$_SESSION['ins_email'] = "";
		$_SESSION['ins_direccion'] = "";
		$_SESSION['ins_telefono'] = "";
		$_SESSION['ins_descripcion'] = "";
	}


}

?>
<?php

//declaración de variables
$regnombre = "";
$regapellido = "";
$regemail = "";
$regemail2 = "";
$regpass = "";
$regpass2 = "";
$regdate = "";
$error_array = array(); //guarda los mensajes de error

if (isset($_POST['reg_button'])) {
	//registro de los valores del form

	//Nombre
	$regnombre = strip_tags($_POST['reg_nombre']);//quita los strip tags que pueden ingresar
	$regnombre = str_replace(' ', '', $regnombre);//quita los espacios
	$regnombre = ucfirst(strtolower($regnombre));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['reg_nombre'] = $regnombre; //guarda el nombre dentro de la variable de sesión

	//apellido
	$regapellido = strip_tags($_POST['reg_apellido']);//quita los strip tags que pueden ingresar
	$regapellido = str_replace(' ', '', $regapellido);//quita los espacios
	$regapellido = ucfirst(strtolower($regapellido));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['reg_apellido'] = $regapellido;

	//email1
	$regemail = strip_tags($_POST['reg_email']);//quita los strip tags que pueden ingresar
	$regemail = str_replace(' ', '', $regemail);//quita los espacios
	//$regemail = ucfirst(strtolower($regemail));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['reg_email'] = $regemail;

	//email2
	$regemail2 = strip_tags($_POST['reg_email2']);//quita los strip tags que pueden ingresar
	$regemail2 = str_replace(' ', '', $regemail2);//quita los espacios
	//$regemail2 = ucfirst(strtolower($regemail2));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['reg_email2'] = $regemail2;

	//password
	$regpass = strip_tags($_POST['reg_password']);//quita los strip tags que pueden ingresar

	//password2
	$regpass2 = strip_tags($_POST['reg_password2']);//quita los strip tags que pueden ingresar

	//date
	$date = date("Y-m-d");//current date

	if ($regemail == $regemail2) {
		//si el memail tiene un formato válido
		if (filter_var($regemail, FILTER_VALIDATE_EMAIL)) {

			$regemail = filter_var($regemail, FILTER_VALIDATE_EMAIL);

			//verificar si el email existe
			$e_check = mysqli_query($con, "SELECT Us_Email FROM afa_usuario WHERE Us_Email='$regemail'");
			//contar el numero de filas devueltas
			$num_filas = mysqli_num_rows($e_check);

			if($num_filas > 0){
				array_push($error_array, "Esta dirección de correo electrónico ya está registrada<br>");
			}
		}else{
			array_push($error_array, "Formato inválido<br>");
		}
	}else{
		array_push($error_array, "Los correo electrónicos no coinciden<br>");
	}

	//nombre debe de tener más de 2 caracteres
	if (strlen($regnombre) > 25 || strlen($regnombre) < 2) {
		array_push($error_array,"El nombre deberia de tener entre 2 y 25 caracteres<br>");		
	}

	//apellido debe de tener más de 2 caracteres
	if (strlen($regapellido) > 25 || strlen($regapellido) < 2) {
		array_push($error_array,"El apellido deberia de tener entre 2 y 25 caracteres<br>");		
	}
	
	if ($regpass != $regpass2) {
		array_push($error_array, "Las contraseñas no coinciden<br>");
	}
	else {
		if (!preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/', $regpass)) {
			array_push($error_array, "<span style='color: red;'>La clave debe contener al menos 8 caracteres, 1 letra mayúscula, una letra minúscula, un carácter numérico y un carácter especial.</span><br>");
		}
	}

	if(strlen($regpass > 50 || strlen($regpass) < 8)) {
		array_push($error_array, "La contraseña debe de tener entre 5 y 30 carácteres<br>");
	}
	
	if (empty($error_array)) {
		$regpass = md5($regpass); //encriptación de passwd
	
		$query = mysqli_query($con, "INSERT INTO afa_usuario VALUES (NULL, '$regnombre', '$regapellido', '$regemail', '$regpass', '$date', 'no')");
		//echo "Error: " . mysqli_error($con);

		array_push($error_array, "<span style='color: #14C800;'>Listo, ya puedes ir al login</span><br>");

		//limpiar las variables de sesión
		$_SESSION['reg_nombre'] = "";
		$_SESSION['reg_apellido'] = "";
		$_SESSION['reg_email'] = "";
		$_SESSION['reg_email2'] = "";
	}


}

?>

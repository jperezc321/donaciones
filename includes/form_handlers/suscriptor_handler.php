<?php
//declaración de variables
$susnombre1 = "";
$susapellido1 = "";
$susemail1 = "";
$tiposuscripcion1 = "";
$susdate1 = "";
$error_array = array(); //guarda los mensajes de error

if (isset($_POST['sus1_button'])) {
	//registro de los valores del form

	//Nombre
	$susnombre1 = strip_tags($_POST['inputNombre']);//quita los strip tags que pueden ingresar
	$susnombre1 = str_replace(' ', '', $susnombre1);//quita los espacios
	$susnombre1 = ucfirst(strtolower($susnombre1));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['inputNombre'] = $susnombre1; //guarda el nombre dentro de la variable de sesión

	//apellido
	$susapellido1 = strip_tags($_POST['inputApellido']);//quita los strip tags que pueden ingresar
	$susapellido1 = str_replace(' ', '', $susapellido1);//quita los espacios
	$susapellido1 = ucfirst(strtolower($susapellido1));//convierte las mayusculas en minusculas solo dejando la primera como susapellido
	$_SESSION['inputApellido'] = $susapellido1;

	//email1
	$susemail1 = strip_tags($_POST['inputEmail']);//quita los strip tags que pueden ingresar
	$susemail = str_replace(' ', '', $susemail1);//quita los espacios
	//$regemail = ucfirst(strtolower($regemail));//convierte las mayusculas en minusculas solo dejando la primera como mayuscula
	$_SESSION['inputEmail'] = $susemail1;

	//tipo suscripcion
	$tiposuscripcion1 = $_POST['tipoSuscripcion'];
	$_SESSION['tipoSuscripcion'] = $tiposuscripcion1;

	//date
	$susdate1 = date("Y-m-d");//current date

	//if ($regemail == $regemail2) {
		//si el memail tiene un formato válido
		if (filter_var($susemail1, FILTER_VALIDATE_EMAIL)) {

			$susemail1 = filter_var($susemail1, FILTER_VALIDATE_EMAIL);

			//verificar si el email existe
			$e_check1_1 = mysqli_query($con, "SELECT Sus_Email FROM AFA_Suscriptor WHERE Sus_Email='$susemail1'");
			//contar el numero de filas devueltas
			$num_filas1_1 = mysqli_num_rows($e_check1_1);

			if($num_filas1_1 > 0){
				array_push($error_array, "Esta dirección de correo electrónico ya está registrada<br>");
			}
		}else{
			array_push($error_array, "Formato inválido<br>");
		}
	//}else{
	//	array_push($error_array, "Los correo electrónicos no coinciden<br>");
	//}

	//nombre debe de tener más de 2 caracteres
	if (strlen($susnombre1) > 25 || strlen($susnombre1) < 2) {
		array_push($error_array,"El nombre deberia de tener entre 2 y 25 caracteres<br>");		
	}

	//apellido debe de tener más de 2 caracteres
	if (strlen($susapellido1) > 25 || strlen($susapellido1) < 2) {
		array_push($error_array,"El apellido deberia de tener entre 2 y 25 caracteres<br>");		
	}
	
	
	if (empty($error_array)) {
	
		$query = mysqli_query($con, "INSERT INTO AFA_Suscriptor VALUES ('', '$susnombre1', '$susapellido1', '$susemail1', '$tiposuscripcion1', '$susdate1', 'Y')");
		//echo "Error: " . mysqli_error($con);

		array_push($error_array, "<span style='color: #14C800;'>Listo, ya puedes ir al login</span><br>");

		//limpiar las variables de sesión
		$_SESSION['inputNombre'] = "";
		$_SESSION['inputApellido'] = "";
		$_SESSION['inputEmail'] = "";
		$_SESSION['tipoSuscripcion'] = "";
	}


}

?>
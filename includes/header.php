<?php
require 'config/config.php';

if (isset($_SESSION['Us_Email'])) {
	$userLoggedIn = $_SESSION['Us_Email'];
	$user_details_query = mysqli_query($con, "SELECT * FROM afa_usuario WHERE Us_Email='$userLoggedIn'");
	$user = mysqli_fetch_array($user_details_query);

}
else{
	header("Location: registro.php");
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>AFA</title>
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/principal_style.css">
	<link rel="stylesheet" type="text/css" href="css/producto_style.css">
	
	<script src="js/producto.js"></script>	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	

   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>


    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark nav-header">
		<a class="navbar-brand" href="principal.php">
    		<img src="images/background/all-x-all.png" width="100" height="30" class="d-inline-block align-top" alt="" loading="lazy">
  		</a>


      <div class="collapse navbar-collapse " id="navbarColor01">
        <ul class="navbar-nav mr-auto">
			<li class="nav-item active">
	        	<a class="nav-link" href="principal.php">Principal <span class="sr-only">(current)</span></a>
	      	</li>

			<li class="nav-item">
	    		<a class="nav-link" href="donacion.php">Donaciones</a>
			</li>
			
			<li class="nav-item">
			    <a class="nav-link" href="suscriptor.php">Suscriptores</a>
			</li>

           	<li class="nav-item">
			    <a class="nav-link" href="intermediario.php">Intermediarios</a>
			</li>

			<li class="nav-item">
			    <a class="nav-link" href="familias.php">Familias</a>
			</li>

			<li class="nav-item">
			    <a class="nav-link" href="producto.php">Productos</a>
			</li>

			<li class="nav-item">
			    <a class="nav-link" href="instituciones.php">Instituciones</a>
			</li>
        </ul>
        <ul class="proflie-dropdown btn-group navbar-nav">
            <li class="nav-item d-inline">
                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<?php echo $user['Us_Nombre'] . " " . $user['Us_Apellido']; ?>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                  
                  <div class="btn-group right" role="group" aria-label="Basic example">
					  
                    <a class="btn btn-danger" style="margin-left: 10px;" href="includes/logout.php">Cerrar Sesion</a>
                  </div>
                </div>
                <!-- <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-start" aria-labelledby="dropdownMenuLink" >

                </div> -->
            </li>
        </ul>
      </div>
    </nav>
  		<!--
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
 		<a class="navbar-brand" href="principal.php">
    		<img src="images/background/all-x-all.png" width="100" height="30" class="d-inline-block align-top" alt="" loading="lazy">
  		</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>
  		<div class="collapse navbar-collapse" id="navbarNav">
	    	<ul class="navbar-nav">
	      		<li class="nav-item active">
	        		<a class="nav-link" href="principal.php">Principal <span class="sr-only">(current)</span></a>
	      		</li>
	      		<li class="nav-item">
	        		<a class="nav-link" href="donacion.php">Donaciones</a>
	      		</li>
			    <li class="nav-item">
			        <a class="nav-link" href="suscriptor.php">Suscriptores</a>
			    </li>
			    <li class="nav-item">
			        <a class="nav-link" href="intermediario.php">Intermediarios</a>
			    </li>
			    <li class="nav-item">
			        <a class="nav-link" href="familias.php">Familias</a>
			    </li>
			    <li class="nav-item">
			        <a class="nav-link" href="producto.php">Productos</a>
			    </li>
			    <li class="nav-item">
			        <a class="nav-link" href="instituciones.php">Instituciones</a>
			    </li>
	    	</ul>
  		</div>	  
		  <select class="nav-link dropdown-toggle" id="Sus_TipoSuscripcion" name="Sus_TipoSuscripcion">
			<option> ?php echo $user['Us_Nombre'] . " " . $user['Us_Apellido']; ?></option>
			<option>
				<a class="dropdown-item" href="includes/logout.php">
  					Cerrar sesión
  				</a>
  			</option>
		</select>
		<ul  class="profile-dropdown btn-group navbar-nav">
		<li class="nav-item d-inline">
		<a style="color: white;" class="btn btn-secondary dropdown-toggle"><?php echo $user['Us_Nombre'] . " " . $user['Us_Apellido'];?></a>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
					<a class="nav-link" href="includes/logout.php">
						Cerrar sesión
					</a>
			</div>
		</li>

		</ul>
	-->
	
  	
	</nav>

</head>
<body>

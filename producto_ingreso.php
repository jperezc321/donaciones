<?php
include("includes/header.php");
//session_destroy();
require 'includes/producto/producto_insert.php';
?>
<!--?php

if (isset($_POST['pro_button'])) {//if reg button is pressed:
		echo '
		<script>
		$(document).ready(function(){
				$("#first").hide();
				$("#second").show();
			});
		</script>
		';
	}
?-->
<link rel="stylesheet" type="text/css" href="css/producto_style.css">
<script src="js/producto.js"></script>

<div class="wrapper">
	<div id="contenedor" class="form_box">
		<div class="modal-header">
	        <h4 class="modal-title" id="exampleModalLabel">Ingresar productos</h4>
      	</div>
		<div class="modal-body">
			<div id="first">
			    <form action="producto_ingreso.php" method="POST">
			       	<div class="form-group">
						<label for="inputNombre"><strong>Nombre</strong></label>
						<input type="text" name="pro_nombre" class="form-control" id="pro_nombre" placeholder="Nombre"
								minlength="2" maxlength="50" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					</div>
					<div class="form-group">
					<label><strong>Tipo de producto</strong></label>
						<select class="form-control" id="pro_tipo" name="pro_tipo" required>
			        		<option value="0">Seleccione:</option>
			        			<?php $query = mysqli_query($con, "SELECT * FROM AFA_TipoProducto");
			          				while ($valores = mysqli_fetch_array($query)) { 
			          					echo '<option value="'.$valores[TP_idTipoProducto].'">'.$valores[TP_TipoProducto].'</option>'; } ?>
			          	</select>
			          	<br>
			          	<label>Si estás ingresando una prenda, llena los siguientes campos:</label>
			        	<!--a href="#" id="registro" class="registro">Hacer click aquí si el producto es una prenda.</a-->
					</div>

					<!-- SOLO SI EL PRODUCTO ES PRENDA -->
					<div > <!--id="second"-->
							<div class="form-row">
								<div class="form-group col-md-6">
						   			<label><strong>Talla</strong></label>
									<select class="form-control" id="pro_talla" name="pro_talla">
						        		<option value="0">Seleccione:</option>
						        			<?php $query = mysqli_query($con, "SELECT * FROM AFA_TallaPrenda");
						          				while ($valores1 = mysqli_fetch_array($query)) { 
						          					echo '<option value="'.$valores1[TPR_idTallaPrenda].'">'.$valores1[TPR_Nombre].'</option>'; } ?>
						          	</select>
						    	</div>
						    	<div class="form-group col-md-6">
						   			<label><strong>Seleccione género</strong></label>					     
								    	<select class="form-control" id="pro_genero" name="pro_genero">
								    		<option value="0">Seleccione:</option>
									      	<option>Mujer</option>
									      	<option>Hombre</option>
									    </select>
						    	</div>
		  					</div>
		  					<div class="form-group">
								<label><strong>Edad</strong></label>
								<select class="form-control" id="pro_edad" name="pro_edad">
					        		<option value="0">Seleccione:</option>
					        			<?php $query = mysqli_query($con, "SELECT * FROM AFA_Edad");
					          				while ($valores2 = mysqli_fetch_array($query)) { 
					          					echo '<option value="'.$valores2[Ed_Edad].'">'.$valores2[Ed_Nombre].'</option>'; } ?>
					          	</select>
			        			<!--a href="#" id="login" class="login">Ver Menos</a-->	
							</div>
					</div>
					<!-- ############################################## -->
					<div class="row">
						<div class="col text-center">
							<input type="submit" name="pro_button" id="pro_button" class="btn btn-info" value="Guardar">
							<a href="producto.php" class="btn btn-dark">Volver</a>
							<br>							
						</div>
					</div> 		
			  	</form>
			</div>
		</div>
	</div>
</div>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>
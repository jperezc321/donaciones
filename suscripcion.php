<?php 
include("includes/main_header.php");

$timezone = date_default_timezone_set("America/Guatemala");

$con = mysqli_connect("localhost", "root", "Z7haOwSi5bMc", "afadb");
$con -> set_charset("utf8");

if (mysqli_connect_errno()) {
  echo "Failed to connect: " . mysqli_connect_errno();
}

require 'includes/suscripcion_handler.php';

$result=""; //indicar si se envió el correo o si hubo error
$result1="";
if (isset($_POST['submit'])) {
    require 'phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->CharSet = "UTF-8";
    $mail->isSMTP();
    $mail->Host='smtp.gmail.com';
    $mail->Port=587;
    $mail->SMTPAuth=true;
    $mail->SMTPSecure='tls';
    $mail->Username='allforall.gt@gmail.com';
    $mail->Password='Jo$$41998';

    $mail->setFrom($_POST['email'],$_POST['nombre']);
    $mail->addAddress('allforall.gt@gmail.com');
    $mail->addReplyTo($_POST['email'],$_POST['nombre']);

    $mail->isHTML(true);
    $mail->Subject='Enviado por '.$_POST['nombre'];
    $mail->Body='<h4>Nombre: '.$_POST['nombre'].'<br>Email: '.$_POST['email'].'<br>Por favor enviar información sobre cómo ser parte de la comunidad como un Intermediario. </h4>';

    if (!$mail->send()) {
        $result="Algo esta mal, intentelo de nuevo";
    }else{
        $result="Gracias por contactarnos, espera la respuesta muy pronto!";
    }
}else if (isset($_POST['sus_button'])) {
    require 'phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;
    $mail->CharSet = "UTF-8";
    $mail->isSMTP();
    $mail->Host='smtp.gmail.com';
    $mail->Port=587;
    $mail->SMTPAuth=true;
    $mail->SMTPSecure='tls';
    $mail->Username='allforall.gt@gmail.com';
    $mail->Password='Jo$$41998';

    $mail->setFrom($_POST['sus_nombre'],$_POST['sus_email'],$_POST['tipo_suscripcion']);
    $mail->addAddress($_POST['sus_email']);
    $mail->addReplyTo($_POST['sus_nombre'],$_POST['sus_email'],$_POST['tipo_suscripcion']);

    $mail->isHTML(true);
    $mail->Subject='¡BIENVENIDO!';
    $mail->Body='<p>Hola '.$_POST['sus_nombre'].', estamos muy felices de que seas parte de esta maravillosa comunidad. Gracias por sumarte como un '.$_POST['tipo_suscripcion'].', juntos haremos una diferencia y brindaremos a las personas en necesidad la calidad de vida que merecen. Espera más noticias muy pronto sobre cómo puedes empezar a bendecir la vida de más personas.</p>';

    if (!$mail->send()) {
        $result1="Algo esta mal, intentelo de nuevo";
    }else{
        $result1="Gracias por suscribirte, espera la respuesta muy pronto!";
    }
}

?>
<meta charset="UTF-8">

<div class="header-inner">
    <div class="container">
      <div class="header-inner-content">
        <h1 class="h1" style="letter-spacing: 1px;">¿Deseas registrarte?</h1>
        <p class="paragraph" style="text-align: justify;">Si deseas ser parte de esa comunidad y ayudar a quienes más lo necesitan puedes registrarte, más abajo encontrarás información sobre los tipos de suscripciones y que conlleva cada una. ¡Gracias por sumarte a este movimiento!</p>
        <a href="#main-content" class="button-3 w-button">Conocer más</a>
      </div>
    </div>
  </div>
  <div class="separator">
    <div class="container"></div>
  </div>
  <main id="main-content" class="main-content">
    <div class="w-container">
      <h2>¿Deseas formar parte de esta comunidad?</h2>
      <p>Nuestro objetivo principal es brindar ayuda a las personas que más lo necesitan, esto puede ser a familias que forman parte de nuestra comunidad así como a instituciones que no reciben el apoyo necesario para atender a las personas que actualmente cuidan. A través de esta plataforma, tú puedes formar parte de está comunidad a través de 3 distintas opciones, las describimos a continuación.</p>
      <h4>1. Donador</h4>
      <p>A través de este primer tipo de suscripción puedes convertirte en un donador constante, esto te permite ayudar mensualmente a una familia y puede ser tanto con víveres como con prendas de vestir, cosas que pueden bendecir la vida de quien más lo necesita. Recibirás a tu correo electrónico notificaciones sobre nuestras &quot;Listas de deseos&quot; mensuales cada vez que estas sean actualizadas para que se te haga más fácil saber con qué puedes colaborar. Puedes suscribirte <a href="#contact-form">aquí</a>.</p>
      <h4>2. Voluntario</h4>
      <p>A través de este tipo de suscripción recibirás notificaciones a tu correo electrónico sobre eventos próximos, en los que podrás apoyarnos tanto con las actividades de entrega de donaciones como con actividades en las que las instituciones a las que apoyamos requieran de personas para hacer voluntariado, ¡Nada mejor que ver sonrisas llenas de agradecimiento! Puedes suscribirte <a href="#contact-form">aquí</a>.</p>
      <h4>3. Intermediario</h4>
      <p>Este tipo de suscripción es solamente para iglesias u organizaciones eclesiásticas que deseen formar parte del movimiento, permitiendo que nuestros donadores puedan entregar en esa ubicación sus donaciones para agilizar el proceso de entrega de donaciones. Si deseas más información, por favor deja tu dirección de correo electrónico en la sección a la que serás redirigido haciendo click <a href="#subscribe-form">aquí</a>.</p>
      <p>No importa cuál tipo de suscripción elijas, ¡Muchísimas gracias por ser parte de esta comunidad y apoyarnos para ayudar a quienes más lo necesitan!</p>
    </div>
  </main>
  <section id="contact-form" class="contact-form">
    <div class="w-container">
      <h2>Formulario de suscripción</h2>
      <p>Ingresa tus datos personales para poder formar parte de esta maravillosa comunidad.</p>
      <div class="w-form">
        <form id="wf-form-Contact-Form" name="wf-form-Contact-Form" data-name="Contact Form" method="POST" action="suscripcion.php">
          <div class="contact-form-grid">
            <div id="w-node-02eeabe6facf-733a8758">
            	<label for="sus_nombre" id="contact-first-name">Nombre</label>
            	<input type="text" class="w-input" maxlength="25" minlength="2" name="sus_nombre" data-name="First Name" id="sus_nombre" value="<?php 
					if (isset($_SESSION['sus_nombre'])) {
						echo $_SESSION['sus_nombre'];
					} ?>" required pattern="[a-zA-Z]{2,25}">
            </div>
            <div id="w-node-02eeabe6fad3-733a8758">
            	<label for="sus_apellido" id="contact-last-name">Apellido</label>
            	<input type="text" class="w-input" maxlength="25" minlength="2" name="sus_apellido" data-name="Last Name" id="sus_apellido" value="<?php 
					if (isset($_SESSION['sus_apellido'])) {
						echo $_SESSION['sus_apellido'];
					} ?>" required pattern="[a-zA-Z]{2,25}">
            </div>
            <div id="w-node-02eeabe6fad7-733a8758">
            	<label for="sus_email" id="contact-email">Correo electrónico</label>
            	<input type="email" class="w-input" maxlength="50" minlength="10" name="sus_email" data-name="Email" id="sus_email" value="<?php 
					if (isset($_SESSION['sus_email'])) {
						echo $_SESSION['sus_email'];
					} ?>" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
				  <?php if (in_array("Esta dirección de correo electrónico ya está registrada<br>", $error_array)) echo "Esta dirección de correo electrónico ya está registrada<br>";
						else if (in_array("Formato inválido<br>", $error_array)) echo "Formato inválido<br>"; ?>
            </div>
            <div id="w-node-02eeabe6fadb-733a8758">
            	<label for="Contact-Phone-Number" id="contact-phone">Tipo de Suscripción</label>
            	<label class="w-radio">
            		<input type="radio" data-name="Radio" id="tipo_suscripcion" name="tipo_suscripcion" value="Donador" class="w-form-formradioinput w-radio-input" required>
            		<span class="w-form-label">Donador</span>
            	</label>
            	<label class="w-radio">
            		<input type="radio" data-name="Radio 2" id="tipo_suscripcion" name="tipo_suscripcion" value="Voluntario" class="w-form-formradioinput w-radio-input" required>
            		<span class="w-form-label">Voluntario</span>
            	</label>
            </div>
          </div>
          <input type="submit" name="sus_button" value="Suscribirse" data-wait="Please wait..." class="submit-button w-button">
        </form>
        <h5> <?= $result1; ?> </h5>
          <div class="w-form-done">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="w-form-fail">
            <div>Oops! Something went wrong while submitting the form.</div>
          </div>
      </div>
    </div>
  </section>

   <section id="subscribe-form" class="subscribe-form">
    <div class="centered-container w-container">
      <h2>¡Conviértete en un intermediario y ayúdanos a ayudar a más personas!</h2>
      <p>Si deseas recibir información sobre cómo convertirte en un intermediario para beneficiar a nuestros donadores ingresa tu dirección de correo electrónico acá y nosotros te estaremos contáctando.</p>
      <div class="w-form">
        
        <form class="subscribe-form-flex" action="suscripcion.php" method="POST">
          <div class="subscribe-form-input-wrapper">
            <label class="field-label">Nombre</label>
            <input type="text" class="text-field-2 w-input" minlength="2" maxlength="50" name="nombre" placeholder="Nombre" id="nombre" required pattern="[a-zA-Z]{2,25}" />
          </div>
          <br>
          &nbsp;
          <div class="subscribe-form-input-wrapper">
            <label for="Subscriber-Email" id="subscribe-email">Correo electrónico</label>
            <input type="email" class="text-field-3 w-input" maxlength="50" minlength="10" name="email" placeholder="Correo electrónico" id="email" pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" required />
          </div>
          &nbsp;
          <input type="submit" name="submit" value="Enviar" data-wait="Please wait..." class="submit-button-3 w-button" style="margin: 10px 8px;"> 
          <br>
          <br>

          
        </form>
        <h5> <?= $result; ?> </h5>

        <!--div class="w-form-done">
          <div>Thank you! Your submission has been received!</div>
        </div>
        <div class="w-form-fail">
          <div>Oops! Something went wrong while submitting the form.</div>
        </div-->
      </div>
    </div>
  </section>

  <div class="footer">
    <div class="container cc-footer">
      <div class="footer-column cc-footer"><a href="index.php" class="navigation-logo w-inline-block"><img src="images/all-x-all-2.png" width="143" srcset="images/all-x-all-2-p-500.png 500w, images/all-x-all-2-p-800.png 800w, images/all-x-all-2.png 1030w" sizes="(max-width: 479px) 30vw, 143px" alt=""></a>
        <div class="text-footer-credits">© 2020 All for All Inc, All rights reserved.</div>
      </div>
      <div class="footer-column">
        <div class="footer-links-list"><a href="index.php" class="link-footer">Inicio</a><a href="about.php" class="link-footer">Conócenos</a><a href="instituciones_info.php" class="link-footer">Instituciones</a><a href="intermediario_info.php" class="link-footer">Intermediarios</a></div>
        <div class="footer-social"><a href="#" class="link-social w-inline-block"><img src="images/icon-facebook.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-twitter.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-instagram.svg" alt=""></a></div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5f571a518e3cdfe11baf4271" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>

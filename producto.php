<?php
include("includes/header.php");
//session_destroy();
require 'includes/producto/producto_insert.php';
require 'includes/producto/producto_update.php';
require 'includes/producto/producto_delete.php';
?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">

<!-- Modal UPDATE ##########################################################################################################-->
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Modificar información de Producto</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="producto.php" method="POST">  
					<div class="form-group">
						<input type="hidden" name="Pro_Producto" id="Pro_Producto">
					</div>
					<div class="form-group">
						<label for="inputNombre"><strong>Nombre</strong></label>
						<input type="text" name="Pro_Nombre" class="form-control" id="Pro_Nombre" placeholder="Nombre"
								minlength="2" maxlength="50" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					</div>
					<div class="form-group">
					<label><strong>Tipo de producto</strong></label>
						<select class="form-control" id="TP_idTipoProducto" name="TP_idTipoProducto">
			        		<option value="val1">Seleccione:</option>
			        			<?php $query = mysqli_query($con, "SELECT * FROM AFA_TipoProducto");
			          				while ($valores = mysqli_fetch_array($query)) { 
			          					echo '<option value="'.$valores['TP_idTipoProducto'].'">'.$valores['TP_TipoProducto'].'</option>'; } ?>
			          	</select>
			        	<!--a href="#" id="registro" class="registro">Hacer click aquí si el producto es una prenda.</a-->
					</div>
					<!-- SOLO SI EL PRODUCTO ES PRENDA -->
					<div>
							<div class="form-row">
								<div class="form-group col-md-6" id="cntTalla">
						   			<label><strong>Talla</strong></label>
									<select class="form-control" id="Pro_PrendaTalla" name="Pro_PrendaTalla">
						        		<option value="0">Seleccione:</option>
						        			<?php $query = mysqli_query($con, "SELECT * FROM AFA_TallaPrenda");
						          				while ($valores1 = mysqli_fetch_array($query)) { 
						          					echo '<option value="'.$valores1['TPR_idTallaPrenda'].'">'.$valores1['TPR_Nombre'].'</option>'; } ?>
						          	</select>
						    	</div>
						    	<div class="form-group col-md-6" id="cntGenero">
						   			<label><strong>Seleccione género</strong></label>					     
								    	<select class="form-control" id="Pro_PrendaGenero" name="Pro_PrendaGenero">
								    		<option value="0">Seleccione:</option>
									      	<option>Mujer</option>
									      	<option>Hombre</option>
									    </select>
						    	</div>
		  					</div>
		  					<div class="form-group" id="cntEdad">
								<label><strong>Edad</strong></label>
								<select class="form-control" id="Pro_PrendaEdad" name="Pro_PrendaEdad">
					        		<option value="0">Seleccione:</option>
					        			<?php $query = mysqli_query($con, "SELECT * FROM AFA_Edad");
					          				while ($valores2 = mysqli_fetch_array($query)) { 
					          					echo '<option value="'.$valores2['Ed_Edad'].'">'.$valores2['Ed_Nombre'].'</option>'; } ?>
					          	</select>
			        			<!--a href="#" id="login" class="login">Ver Menos</a-->	
							</div>
					</div>
				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="pro_update" id="pro_update" class="btn btn-info" value="Actualizar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal UPDATE ###################################################################################################-->

<!-- Modal 3 DELETE ##########################################################################################################-->
<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Eliminar información de producto</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="producto.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Pro_Producto_del" id="Pro_Producto_del">
					</div>
					
					<h5>¿Deseas eliminar este dato?</h5>

				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="pro_delete" id="pro_delete" class="btn btn-info" value="Eliminar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 3 DELETE ####################################################################################################-->




<div>
	<div class="header">
		<h1>Productos</h1>
		<p>
			<a href="producto_ingreso.php" class="btn btn-outline-dark btn-sm">+ Agregar nuevo</a>
		</p>
	</div>
	<div class="container_donacion">
		<?php 
			$query_run = mysqli_query($con, "
			SELECT AFAP.Pro_Producto, AFAP.Pro_Nombre, AFATP.TP_TipoProducto, AFAP.Pro_EstadoData, 
			ATP.TPR_Nombre, AFAP.Pro_PrendaGenero, AFD.Ed_Nombre, SUM(AFDD.DDR_Cantidad) AS Inventario
			FROM afa_producto AFAP 
			INNER JOIN afa_tipoproducto AFATP ON AFAP.TP_idTipoProducto = AFATP.TP_idTipoProducto 
			LEFT JOIN afa_tallaprenda ATP ON ATP.TPR_idTallaPrenda = AFAP.Pro_PrendaTalla 
			LEFT JOIN afa_edad AFD ON AFD.Ed_Edad = AFAP.Pro_PrendaEdad 
			LEFT JOIN afa_detalle_donacionrecibida AFDD 
			ON AFDD.DDR_Producto = AFAP.Pro_Producto
			WHERE AFAP.Pro_EstadoData = 'Y'
			GROUP BY AFAP.Pro_Producto
			ORDER BY AFAP.Pro_Producto ASC
			");
		?>
		<table class="table table-hover display" id="tabla1" style="width:100%">
		  <thead>
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">Producto</th>
		      <th scope="col">Tipo de producto</th>
		      <th scope="col">Talla</th>
		      <th scope="col">Genero</th>
			  <th scope="col">Edad</th>
			  <th scope="col">Inventario</th>
		      <th scope="col">Opciones</th>
		    </tr>
		  </thead>
		 
		  <tbody>
		  	 <?php
			if ($query_run) {
				foreach ($query_run as $row){
			?>
		    <tr>
		      	<td> <?php echo $row['Pro_Producto'];?></td>
			    <td> <?php echo $row['Pro_Nombre'];?> </td>
			    <td> <?php echo $row['TP_TipoProducto'];?> </td>
			    <td> <?php echo isset($row['TPR_Nombre'])?$row['TPR_Nombre']:"N/A"  ?> </td>
			    <td> <?php echo isset($row['Pro_PrendaGenero'])?$row['Pro_PrendaGenero']:"N/A"?> </td>
				<td> <?php echo isset($row['Ed_Nombre'])?$row['Ed_Nombre']:"N/A"?> </td>
				<td> <?php echo isset($row['Inventario'])?$row['Inventario']:"0"?> </td>
			    <td>
			    	<button type="button" class="btn btn-outline-info editbtn">Modificar</button>
			    	<button type="button" class="btn btn-outline-danger deletebtn">Eliminar</button>
			    	
			    </td>
		    </tr>
		     <?php
				}
			}else{
				echo "No se encontraron registros";
			}
		?>
		  </tbody>
		 
		</table>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<!-- SCRIPT PARA VER EL MODAL DE DELETE -->
<script>
		$(document).ready(function() {
    		$('#tabla1').DataTable();
		} );
	
	$(document).ready(function (){
		$('.deletebtn').on('click', function() {
			$('#deletemodal').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();

			console.log(data[0]);

			$('#Pro_Producto_del').val(data[0]);
		});
	});
</script>

<!-- SCRIPT PARA VER EL MODAL DE UPDATE -->
<script>

	
	$(document).ready(function (){
		$('.editbtn').on('click', function() {
			$('#editmodal').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();

			$('#Pro_Producto').val(data[0]);
			$('#Pro_Nombre').val(data[1].trim());
			$('#TP_idTipoProducto').val(data[3]);
			$('#Pro_PrendaTalla').val(data[6]);
			$('#Pro_PrendaGenero').val(data[7]);
			$('#Pro_PrendaEdad').val(data[8]);

			<?php 
			$info = mysqli_query($con,"SELECT * FROM afa_producto ");
			while ($row = mysqli_fetch_array($info)) { 
				$arrInfo[$row["Pro_Producto"]]["Pro_Producto"] =  $row["Pro_Producto"];
				$arrInfo[$row["Pro_Producto"]]["Pro_Nombre"] =  $row["Pro_Nombre"];
				$arrInfo[$row["Pro_Producto"]]["TP_idTipoProducto"] =  $row["TP_idTipoProducto"];
				$arrInfo[$row["Pro_Producto"]]["Pro_PrendaTalla"] =  $row["Pro_PrendaTalla"];
				$arrInfo[$row["Pro_Producto"]]["Pro_PrendaGenero"] =  $row["Pro_PrendaGenero"];
				$arrInfo[$row["Pro_Producto"]]["Pro_PrendaEdad"] =  $row["Pro_PrendaEdad"];
			}
			?>
			var informacion = <?php echo json_encode($arrInfo) ?>;
			$.each(informacion, function(index,contenido){
			
				if(contenido.Pro_Producto == data[0].trim()){
					$("#TP_idTipoProducto").val(contenido.TP_idTipoProducto);
					$("#Pro_Nombre").val(contenido.Pro_Nombre);
					if(contenido.Pro_PrendaTalla){
						$("#cntTalla").show();
						$("#Pro_PrendaTalla").val(contenido.Pro_PrendaTalla);
					} else {
						$("#cntTalla").hide();
					}
					if(contenido.Pro_PrendaGenero){
						$("#cntGenero").show();
						$("#Pro_PrendaGenero").val(contenido.Pro_PrendaGenero);
					} else {
						$("#cntGenero").hide();
					}
					if(contenido.Pro_PrendaEdad){
						$("#cntEdad").show();
						$("#Pro_PrendaEdad").val(contenido.Pro_PrendaEdad);
					} else {
						$("#cntEdad").hide();
					}
					console.log(contenido);
				}
			
		
			})

			

		});
	});
</script>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>
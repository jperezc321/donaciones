<?php
include("includes/header.php");
//session_destroy();
?>

<div class="card-deck" style="margin: 2%">

            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="images/mainpage/donation.png" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">Donaciones</h5>
                <p class="card-text">Ingreso y control de las donaciones recibidas.</p>
                <br>
                <a href="donacion.php" class="btn btn-dark">Ir</a>
              </div>
            </div>


            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="images/mainpage/user.png" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">Suscriptores</h5>
                <p class="card-text">Información tanto de donadores como voluntarios.</p>
                <a href="suscriptor.php" class="btn btn-dark">Ir</a>
              </div>
          </div>


            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="images/mainpage/church.png" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">Intermediarios</h5>
                <p class="card-text">Información sobre nuestros intermediarios.</p>
                <br>
                <a href="intermediario.php" class="btn btn-dark">Ir</a>
              </div>
            </div>


            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="images/mainpage/family.png" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">Familias beneficiadas</h5>
                <p class="card-text">Información sobre las familias beneficiadas.</p>
                <a href="familias.php" class="btn btn-dark">Ir</a>
              </div>
            </div>

            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="images/mainpage/grocery.png" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">Productos</h5>
                <p class="card-text">Listado de productos recibidos en las donaciones.</p>
                <a href="producto.php" class="btn btn-dark">Ir</a>
              </div>
            </div>
</div>

<div class="card-deck" style="width: 57.5%; margin: 2%">            

            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="images/mainpage/cuidar.png" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">Instituciones</h5>
                <p class="card-text">Listado de instituciones en la página web.</p>
                <br>
                <a href="instituciones.php" class="btn btn-dark">Ir</a>
              </div>
            </div>

            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="images/mainpage/share.png" alt="Card image cap" >
              <div class="card-body">
                <h5 class="card-title">Donaciones entregadas</h5>
                <p class="card-text">Listado de donaciones entregadas.</p>
                <a href="donacion_entregada.php" class="btn btn-dark">Ir</a>
              </div>
            </div>

            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="images/mainpage/application.png" alt="Card image cap" style="margin: 4% 1% 1% 1%">
              <div class="card-body">
                <h5 class="card-title">Reportes</h5>
                <p class="card-text">Reportes existentes.</p>
                <br>
                <br>
                <a href="reportes.php" class="btn btn-dark">Ir</a>
              </div>
            </div>
       
</div>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>

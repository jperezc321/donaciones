<?php
include("includes/header.php");
//session_destroy();

?>

<div>
	<div class="header">
		<h1>Donaciones entregadas</h1>		
	</div>
	<div class="container_donacion">
		<?php 
			$query_run = mysqli_query($con, "SELECT AFAD.Don_Donacion, AFAD.Don_FechaEntrega, AFAD.Us_UsuarioEntrega, AFAF.Fam_Apellidos, AFAD.Don_Estado FROM afa_donacion AFAD 
				INNER JOIN afa_familia AFAF
				ON AFAF.Fam_Familia = AFAD.Fam_Familia
				WHERE AFAD.Don_EstadoData = 'Y'
				AND AFAD.Don_Estado = 'Entregada'
				ORDER BY AFAD.Don_Donacion
				");
		?>
		<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">Fecha que entrega</th>
		      <th scope="col">Usuario que entrega</th>
		      <th scope="col">Familia que recibe</th>
		      <th scope="col">Estado</th>
		      <th scope="col">Opciones </th>
		    </tr>
		  </thead>
		  <?php
			if ($query_run) {
				foreach ($query_run as $row){
		?>
		  <tbody>
		    <tr>
		      <td> <?php echo $row['Don_Donacion'];?></td>
			    <td> <?php echo $row['Don_FechaEntrega'];?> </td>
			    <td> <?php echo $row['Us_UsuarioEntrega'];?> </td>
			    <td> <?php echo $row['Fam_Apellidos'];?> </td>
			    <td> <?php echo $row['Don_Estado'];?> </td>
			    <td>
			    	<a class="btn btn-outline-info editbtn" style="text-decoration:none;" href="<?php print "detalle.php?id=".$row['Don_Donacion']; ?>">Ver Detalle</a>
			    	<!--button type="button" class="btn btn-outline-danger deletebtn">Eliminar</button-->
			    	<a class="btn btn-outline-secondary" style="text-decoration:none;" href="<?php print "donacion_ent_consulta.php?id=".$row['Don_Donacion']; ?>">Ver más</a>
			    </td>
		    </tr>
		  </tbody>
		  <?php
				}
			}else{
				echo "No se encontraron registros";
			}
		?>
		</table>
	</div>
</div>


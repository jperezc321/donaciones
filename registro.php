<?php
require 'config/config.php';
require 'includes/form_handlers/registro_handlers.php';
require 'includes/form_handlers/login_handler.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Registro</title>
	<link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/registro_style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="js/register.js"></script>

</head>
<body>

	<?php 

	if (isset($_POST['reg_button'])) {//if reg button is pressed:
		echo '
		<script>
		$(document).ready(function(){
				$("#first").hide();
				$("#second").show();
			});
		</script>
		';
	}

	?>

	<div class="wrapper">
		<div id="contenedor" class="login_box">
			<div class="login_header">
				<h1>all for all</h1>
			</div>

			<div id="first">

				<form action="registro.php" method="POST">
					<input type="email" name="log_email" placeholder="Correo Electrónico" value="<?php 
					if (isset($_SESSION['log_email'])) {
					echo $_SESSION['log_email'];
					} ?>" required>
					<br>
					<input type="password" name="log_password" placeholder="Contraseña">
					<br>
					<?php if (in_array("El correo electronico o la contraseña está incorrecto<br>", $error_array)) echo "El correo electronico o la contraseña está incorrecto<br>"; ?>
					<input class="btn btn-secondary" type="submit" name="log_button" value="Ingresar">
					<br>
					<a href="#" id="registro" class="registro">Necesitas una cuenta? Registrate haciendo click aquí.</a>
				</form>

			</div>
			
			<div id="second">
				
				<form action="registro.php" method="POST">
					<input type="text" name="reg_nombre" placeholder="Nombre" value="<?php 
					if (isset($_SESSION['reg_nombre'])) {
						echo $_SESSION['reg_nombre'];
					} ?>" required>
					<br>
					<?php if (in_array("El nombre deberia de tener entre 2 y 25 caracteres<br>", $error_array)) echo "El nombre deberia de tener entre 2 y 25 caracteres<br>"; ?>

					<input type="text" name="reg_apellido" placeholder="Apellido" value="<?php 
					if (isset($_SESSION['reg_apellido'])) {
						echo $_SESSION['reg_apellido'];
					} ?>" required>
					<br>
					<?php if (in_array("El apellido deberia de tener entre 2 y 25 caracteres<br>", $error_array)) echo "El apellido deberia de tener entre 2 y 25 caracteres<br>"; ?>


					<input type="email" name="reg_email" placeholder="Correo electronico" value="<?php 
					if (isset($_SESSION['reg_email'])) {
						echo $_SESSION['reg_email'];
					} ?>" required>
					<br>

					<input type="email" name="reg_email2" placeholder="Confirme correo electronico" value="<?php 
					if (isset($_SESSION['reg_email2'])) {
						echo $_SESSION['reg_email2'];
					} ?>" required>
					<br>
					<?php if (in_array("Esta dirección de correo electrónico ya está registrada<br>", $error_array)) echo "Esta dirección de correo electrónico ya está registrada<br>";
						else if (in_array("Formato inválido<br>", $error_array)) echo "Formato inválido<br>"; 
						else if (in_array("Los correo electrónicos no coinciden<br>", $error_array)) echo "Los correo electrónicos no coinciden<br>"; ?>

					<input type="password" name="reg_password" placeholder="Contraseña" required>
					<br>
					<input type="password" name="reg_password2" placeholder="Confirme contraseña" required>
					<br>
					<?php if (in_array("Las contraseñas no coinciden<br>", $error_array)) echo "Las contraseñas no coinciden<br>";
						else if (in_array("<span style='color: red;'>La clave debe contener al menos 8 caracteres, 1 letra mayúscula, una letra minúscula, un carácter numérico y un carácter especial.</span><br>", $error_array)) echo "<span style='color: red;'>La clave debe contener al menos 8 caracteres, 1 letra mayúscula, una letra minúscula, un carácter numérico y un carácter especial.</span><br>"; 
						else if (in_array("La contraseña debe de tener entre 5 y 30 carácteres<br>", $error_array)) echo "La contraseña debe de tener entre 5 y 30 carácteres<br>"; ?>
					
					<input class="btn btn-secondary" type="submit" name="reg_button" value="Registrar">	
					<br>
					<?php if (in_array("<span style='color: #14C800;'>Listo, ya puedes ir al login</span><br>", $error_array)) echo "<span style='color: #14C800;'>Listo, ya puedes ir al login</span><br>"; ?>
					<a href="#" id="login" class="login">Ya tienes una cuenta? Ingresa aquí.</a>
				</form>	

			</div>

		</div>


	</div>

</body>
</html>

<?php
include("includes/header.php");
require 'includes/detalle/detalle_insert.php';

?>

<div>
	<div id="contenedor" class="form_box">
		<div class="modal-header">
	        <h4 class="modal-title" id="exampleModalLabel">Detalle de donación</h4>
      	</div>
		<div class="modal-body">
			<div id="first">
			    <form action="detalle_ingreso.php" method="POST">

			    	<div class="form-row">
			    		<div class="form-group col-md-6">
			    			<input type="hidden" name="idDonacion" value="<?php print $_GET["id"] ?>"> 
			    			<input type="hidden" name="contador" id="contador" value="0">
			    			
			    		</div>
			    	</div>
					<div class="form-row">						
						<div class="form-group col-md-6">
						   	<label><strong>Producto</strong></label>
							<select class="form-control" id="det_producto" name="det_producto">
			        			<option value="0">Seleccione:</option>
			        			<?php $query = mysqli_query($con, "SELECT afap.Pro_Producto, afap.Pro_Nombre, afatp.TPR_Nombre, afae.Ed_Nombre, afap.Pro_PrendaGenero FROM afa_producto afap
									LEFT JOIN afa_tallaprenda afatp
									ON afatp.TPR_idTallaPrenda = afap.Pro_PrendaTalla
									LEFT JOIN afa_edad afae
									ON afae.Ed_Edad = afap.Pro_PrendaEdad;");
			          			while ($valores = mysqli_fetch_array($query)) { 

			          			echo '<option value="'.$valores[Pro_Producto].'">'.$valores[Pro_Nombre].' '.$valores[TPR_Nombre].' '.$valores[afae.Ed_Nombre].' '.$valores[Pro_PrendaGenero].'</option>'; } ?>
			          		</select>	
						</div>
						<div class="form-group col-md-4">						   
							<label for="inputNombre"><strong>Cantidad</strong></label>
							<input type="text" name="det_cantidad" class="form-control" id="det_cantidad">					
						</div>
						<div class="form-group col-md-2" >
							<label><b>Añadir</b></label>
							<br>
							<div class="btn btn-success" onclick="nuevoProducto();">+</div>
						</div>
		  			</div>
		  		

					

		  			<div class="form-row">	
		  				<table class="table" id="tblProductos">
		  					<thead>
		  						<th>Producto</th>
								<th>Cantidad</th>
								<th>Fecha de vencimiento</th>
		  					</thead>
		  				</table>
		  			</div>
					<div class="row">
						<div class="col text-center">
							<input type="submit" name="don_button" id="don_button" class="btn btn-info" value="Guardar">
							<a href="donacion.php" class="btn btn-dark">Volver</a>
							<br>							
						</div>
					</div> 		
			  	</form>
			</div>
		</div>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>

<script>

	function nuevoProducto(){
		contador = $("#contador").val();
		producto = $("#det_producto option:selected").text();
		productoId = $("#det_producto option:selected").val();
		cantidad = $("#det_cantidad").val();
		

		$("#tblProductos").append("<tbody>");
		$("#tblProductos").append("<tr>");
		$("#tblProductos").append("<td>"+producto+"<input type='hidden' name ='pr"+contador+"' value='"+productoId+"' ></td>");
		$("#tblProductos").append("<td>"+cantidad+"<input type='hidden' name ='ct"+contador+"' value='"+cantidad+"' ></td>");
		$("#tblProductos").append("<td><input type='date' class='form-control' name='fecha_vencimiento"+contador+"' id='fecha_vencimiento'></td>");
		$("#tblProductos").append("</tr>");
		$("#tblProductos").append("</tbody>");
		contador++;
		$("#contador").val(contador);
	}
</script>
</html>

<?php
	include("includes/main_header.php");

?>

<div class="header-inner">
    <div class="container">
      <div class="header-inner-content">
        <h1 class="h1" style="letter-spacing: 2px;">¿Quiénes somos?</h1>
        <p class="paragraph" style="text-align: justify;">Somos una comunidad enfocada en ayudar a familias de escasos recursos y, además, dar visibilidad a la necesidad que existe en instituciones y organizaciones que no cuentan con los recursos suficientes para darse a conocer en la web. </p><a href="#hero" class="button-3 w-button">Conocer más</a></div>
    </div>
  </div>
  <header id="hero" class="hero-2" style="background-color: #eae6ca;">
    <div class="flex-container-5 w-container">
      <div>
        <h1 class="h1" style="color: black; margin-right: 400px">¿Qué hacemos?</h1>
        <p class="paragraph-9" style="text-align: justify;">Nuestro objetivo principal es el de ayudar familias de escasos recursos que están ubicadas en el municipio de Villa Nueva en Guatemala. Lo hacemos a través de la ayuda que nuestros donadores nos brindan. Cada persona que decide suscribirse como donador está ayudando a una familia específicamente a través de donaciones mensuales de víveres y prendas. Estas donaciones son hechas por medio de nuestros Intermediarios, que son organizaciones eclesiásticas que nos han abierto sus puertas para que nuestros donadores puedan ir a dejar sus donaciones a la ubicación que más les favorezca. <br><br>Cuando te suscribes como un donador estás ayudando a que una familia que actualmente vive en necesidad, pueda tener una mejor calidad de vida. Si deseas formar parte de esta comunidad, puedes suscribirte <a href="suscripcion.php">aquí</a>.</p>
      </div>
      <div class="hero-image-mask"></div>
    </div>
  </header>
  <section id="vision-mision" class="feature-section-3">
    <div class="flex-container-3 w-container">
      <div class="feature-image-mask"><img src="images/18923-min-copy.jpg" srcset="images/18923-min-copy-p-500.jpeg 500w, images/18923-min-copy-p-1080.jpeg 1080w, images/18923-min-copy-p-1600.jpeg 1600w, images/18923-min-copy-p-2000.jpeg 2000w, images/18923-min-copy-p-2600.jpeg 2600w, images/18923-min-copy-p-3200.jpeg 3200w, images/18923-min-copy.jpg 6195w" sizes="(max-width: 479px) 82vw, (max-width: 767px) 85vw, (max-width: 991px) 37vw, 39vw" alt="" class="feature-image-2"></div>
      <div class="div-block-2">
        <h2>Visión</h2>
        <p class="paragraph-4" style="text-align: justify;">Ayudar a las familias de escasos recursos que residen en el municipio de Villa Nueva en Guatemala. Asimismo, dar visibilidad a las instituciones y organizaciones que no cuentan con los recursos suficientes para poder darse a conocer a través de la web.</p>
        <h2>Misión</h2>
        <p class="paragraph-5" style="text-align: justify;">Construir una comunidad a través de la página web, brindando la opción a los miembros de suscribirse como donadores y voluntarios. Asimismo, proveer de una plataforma a distintas organizaciones e instituciones enfocadas en brindar ayuda social. <br></p>
      </div>
    </div>
  </section>
  <div class="premium cc-background">
    <div class="container">
      <div class="row cc-reverse">
        <div class="_2-row-image cc-row-reverse"><img src="images/13418-min-1.jpg" alt="" class="image-3"></div>
        <div class="_2-row-text cc-row-reverse">
          <h2 class="heading-4">¿Deseas contactarnos?</h2>
          <p class="paragraph cc-gray" style="text-align: justify;">Puedes enviarnos un correo electrónico a la dirección: <strong>alltoall.gt@gmail.com</strong></p>
        </div>
      </div>
    </div>
  </div>
  <div class="footer">
    <div class="container cc-footer">
      <div class="footer-column cc-footer"><a href="index.php" class="navigation-logo w-inline-block"><img src="images/all-x-all-2.png" width="143" srcset="images/all-x-all-2-p-500.png 500w, images/all-x-all-2-p-800.png 800w, images/all-x-all-2.png 1030w" sizes="(max-width: 479px) 30vw, 143px" alt=""></a>
        <div class="text-footer-credits">© 2020 All for All Inc, All rights reserved.</div>
      </div>
      <div class="footer-column">
        <div class="footer-links-list"><a href="index.php" class="link-footer">Inicio</a><a href="about.php" aria-current="page" class="link-footer w--current">Conócenos</a><a href="instituciones_info.php" class="link-footer">Instituciones</a><a href="intermediario_info.php" class="link-footer">Intermediarios</a></div>
        <div class="footer-social"><a href="#" class="link-social w-inline-block"><img src="images/icon-facebook.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-twitter.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-instagram.svg" alt=""></a></div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5f571a518e3cdfe11baf4271" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>
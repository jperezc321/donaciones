<?php

include("includes/header.php");
ini_set('error_reporting', E_ALL);
//$con = mysqli_connect("localhost", "root", "Z7haOwSi5bMc", "afadb");
//session_destroy();
$id = isset($_GET['id'])?$_GET['id']:0;
$modificar = isset($_POST['modificar'])?$_POST['modificar']:'';
if($modificar == 'Y'){
	$idMod = $_POST['idMod'];
	$cantidad = $_POST["cantidad"];
	$fecha = isset($_POST["fecha"])?$_POST["fecha"]: "";
	if($fecha == ''){
		$query_run = mysqli_query($con, "UPDATE afa_detalle_donacionrecibida AFAD SET AFAD.DDR_Cantidad = {$cantidad} WHERE AFAD.DDR_DetalleDR = {$idMod}");
	} else {
		$query_run = mysqli_query($con, "UPDATE afa_detalle_donacionrecibida AFAD SET AFAD.DDR_Cantidad = {$cantidad}, AFAD.DDR_FechaVencimiento = '{$fecha}' WHERE AFAD.DDR_DetalleDR = {$idMod}");
	}


}
$eliminar = isset($_POST['eliminar'])?$_POST['eliminar']:'';
if($eliminar== 'Y'){
	$idMod = $_POST['idMod'];
	$query_run = mysqli_query($con, "UPDATE afa_detalle_donacionrecibida AFAD SET AFAD.DDR_EstadoData = 'N' WHERE AFAD.DDR_DetalleDR = {$idMod}");
}

$nuevo = isset($_POST['crear'])?$_POST['crear']:'';
if($nuevo== 'Y'){
	$id = $_POST["donacion"];
	$producto = $_POST['producto'];
	$cantidad = $_POST["cantidad"];
	$fecha = $_POST["fecha"];
	$query = "INSERT INTO afa_detalle_donacionrecibida VALUES ('', '$cantidad', '$producto', $id,'$fecha', 'Y')";
	$query_run = mysqli_query($con, $query);
}
?>

<div>
	<div class="header">
		<h1>Detalle</h1>
	</div>
	<div class="container_donacion">
		<?php 
			$query = "SELECT AFADetalle.DDR_DetalleDR, AFADetalle.DDR_Cantidad, AFAP.Pro_Nombre, AFADetalle.Don_Donacion,
			IF(DATE(AFADetalle.DDR_FechaVencimiento) != '0000-00-00',DATE(AFADetalle.DDR_FechaVencimiento),'N/A') AS fecha
			 FROM 
						AFA_Detalle_donacionrecibida AFADetalle
						INNER JOIN afa_producto AFAP 
						ON AFADetalle.DDR_Producto = AFAP.Pro_Producto
						WHERE AFADetalle.Don_Donacion = {$id}
						AND AFADetalle.DDR_EstadoData = 'Y';";
			$query_run = mysqli_query($con,$query);
			print $query;
		
		
	?>
		   
	 <div type="button" class="btn btn-outline-success addbtn" onclick="nuevoProducto();">+</div>
		<br> <br>
		<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">ID</th>		      
		      <th scope="col">Cantidad</th>
			  <th scope="col">Producto</th>
			  <th scope="col">Fecha vencimiento</th>
			  <th scope="col">Opciones</th>
		    </tr>
		  </thead>
		  <tbody  id='productos'>
		  <input type="hidden" name="contador" id="contador" value="0">
		  <input type="hidden"  id="tmpid" value="<?php print $id; ?>">
		  	 <?php
			if ($query_run) {
				foreach ($query_run as $row){
			      
			?>
			
		    <tr>
		      	<td> <input type="text" value="<?php echo $row['DDR_DetalleDR'];?>" name="id<?php echo $row['DDR_DetalleDR'];?>" disabled></td>
			    <td> <input type="text" value="<?php echo $row['DDR_Cantidad'];?>" id="cantidad<?php echo $row['DDR_DetalleDR'];?>" disabled> </td>
				<td> <input type="text" value="<?php echo $row['Pro_Nombre'];?>" id="producto<?php echo $row['DDR_DetalleDR'];?>" disabled> </td>
				<?php if( $row['fecha'] =='N/A') {?>
					<td> N/A </td>
				<?php } else {?>
				<td> <input type="date" value="<?php echo $row['fecha'];?>" id="fecha<?php echo $row['DDR_DetalleDR'];?>" disabled> </td>
				<?php } ?>
				<td>
					<div type="button" class="btn btn-outline-info" id="modificar<?php echo $row['DDR_DetalleDR'];?>" onclick="habilitar(<?php echo $row['DDR_DetalleDR'];?>);">Modificar</div>
			    	<div type="button" class="btn btn-outline-info editbtn" id="guardar<?php echo $row['DDR_DetalleDR'];?>" onclick="modificar(<?php echo $row['DDR_DetalleDR'];?>)" style="display: none;">Guardar</div>
			    	<div type="button" class="btn btn-outline-danger deletebtn" onclick="eliminar(<?php echo $row['DDR_DetalleDR'];?>)">Eliminar</div>
			   
			    </td>
		    </tr>
		     <?php
				}
			}else{
				echo "No se encontraron registros";
			}
		?>
		  </tbody>
		 
		</table>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
<script>
	function habilitar(id){
		$("#cantidad"+id).prop( "disabled", false );
		if($("#fecha"+id)){
			$("#fecha"+id).prop( "disabled", false );
		}
		$("#modificar"+id).hide();
		$("#guardar"+id).show();
	}
	function modificar(id){
		if($("#fecha"+id).val()){
			var param = {
    		idMod: id,
			cantidad: $("#cantidad"+id).val(),
			fecha: $("#fecha"+id).val(),
			modificar: 'Y'
			};
		} else {
			var param = {
    		idMod: id,
			cantidad: $("#cantidad"+id).val(),
			modificar: 'Y'
			};
		}
	
		$.ajax({
    	 data: param,
    	 url: "detalle.php",
    	 method: "post",
    	 success: function(data) {
    	     
		 }
		});

		$("#modificar"+id).show();
		$("#guardar"+id).hide();
		$("#cantidad"+id).prop( "disabled", true);
		$("#fecha"+id).prop( "disabled", true );
	}

	function eliminar(id){
		var param = {
    		idMod: id,
			eliminar: 'Y'
		};
		$.ajax({
    	 data: param,
    	 url: "detalle.php",
    	 method: "post",
    	 success: function(data) {
    	     
		 }
		});
		
		location.reload();
	}


	function nuevoProducto(){
		contador = $("#contador").val();
		producto = $("#det_producto option:selected").text();
		productoId = $("#det_producto option:selected").val();
		cantidad = $("#det_cantidad").val();
		var tmpcontador =contador;

		$("#productos").append("<tr>");
		$("#productos").append("<td><input type='text'  id ='idtmp"+contador+"' name ='idtmp"+contador+"' disabled></td>");
		$("#productos").append("<td><input type='text' id='cnttmp"+contador+"' name ='cnttmp"+contador+"' ></td>");
		$("#productos").append(`<td>		
		<select class="form-control" id="det_producto`+contador+`" name="det_producto`+contador+`">
			        			<option value="0">Seleccione:</option>
			        			<?php $query = mysqli_query($con, "SELECT afap.Pro_Producto, afap.Pro_Nombre, afatp.TPR_Nombre, afae.Ed_Nombre, afap.Pro_PrendaGenero FROM afa_producto afap LEFT JOIN afa_tallaprenda afatp ON afatp.TPR_idTallaPrenda = afap.Pro_PrendaTalla LEFT JOIN afa_edad afae ON afae.Ed_Edad = afap.Pro_PrendaEdad;");
			          			while ($valores = mysqli_fetch_array($query)) { 

			          			echo '<option value="'.$valores['Pro_Producto'].'">'.$valores['Pro_Nombre'].' '.$valores['TPR_Nombre'].' '.$valores['Ed_Nombre'].' '.$valores['Pro_PrendaGenero'].'</option>'; } ?>
			          		</select>
							  </td>`);
	    $("#productos").append("<td><input type='date' id='fecha"+contador+"' name ='fecha"+contador+"' ></td>");
		$("#productos").append(`<td><div type='button' class='btn btn-outline-info editbtn' onclick="guardarNuevo(`+tmpcontador+`)">Guardar</div></td>`);
		$("#productos").append("</tr>")

		contador++;
		$("#contador").val(contador);
	}
	
	function guardarNuevo(contador){
	
		producto = $("#det_producto"+contador).val();
		cantidad = $("#cnttmp"+contador).val();
		fecha = $("#fecha"+contador).val();
		donacion = $("#tmpid").val();
		var param = {
    		producto: producto,
			cantidad: cantidad,
			donacion: donacion,
			fecha: fecha,
			crear: 'Y'
		};
		$.ajax({
			data: param,
			url: "detalle.php",
			method: "post",
			success: function(data) {
				
			}
		});
		
		$("#det_producto"+contador).prop( "disabled", true );
		 $("#cnttmp"+contador).prop( "disabled", true );
		 if(fecha){
			$("#fecha"+contador).prop( "disabled", true );
		 } else {
			$("#fecha"+contador).hide();
		 }
	
		$("#idtmp"+contador).val(	<?php $query = mysqli_query($con, "SELECT AFAD.DDR_DetalleDR FROM afa_detalle_donacionrecibida AFAD ORDER BY AFAD.DDR_DetalleDR DESC LIMIT 1");
			          			while ($valores = mysqli_fetch_array($query)) { 

			          			echo intVal($valores['DDR_DetalleDR']) + 1; } ?>);
								  
	}
</script>
</html>

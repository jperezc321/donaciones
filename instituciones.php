<?php
include("includes/header.php");
require 'includes/institucion/institucion_insert.php';
require 'includes/institucion/institucion_update.php';
require 'includes/institucion/institucion_delete.php';
?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Ingresar información de Institución</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       	<form action="instituciones.php" method="POST">
  
			<div class="form-group">
					<label for="inputNombre"><strong>Nombre</strong></label>
					<input type="text" name="ins_nombre" class="form-control" id="inputNombre" placeholder="Nombre"
					minlength="2" maxlength="50" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales">
			</div>
			<div class="form-group">
				<label for="inputEmail"><strong>Correo electrónico</strong></label>
				<input type="email" name="ins_email" class="form-control" id="inputEmail" placeholder="Correo electrónico" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" title="Ingresa la dirección de correo electrónico completa.">
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputDireccion"><strong>Dirección</strong></label>
					<input type="text" name="ins_direccion" class="form-control" id="inputDireccion" placeholder="Dirección" minlength="5" maxlength="50" required>
				</div>
				<div class="form-group col-md-6">
					<label for="inputTel"><strong>Número de teléfono</strong></label>
					<input type="text" name="ins_telefono" class="form-control" id="inputTel" placeholder="Número de teléfono" minlength="8" maxlength="8" required pattern="^\d*$" title="Solo se aceptan números.">
				</div>
			</div>
			<div class="form-group">
			    <label><strong>Descripción de Institución</strong></label>
			    <textarea name="ins_descripcion" class="form-control" id="txtareaDescripcion" rows="3" placeholder="Descripción" minlength="5" maxlength="255"></textarea>
			</div>
		  	<div class="row">
				<div class="col text-center">
					<input type="submit" name="ins_button" id="ins_button" class="btn btn-info" value="Guardar">
					<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
				</div>
			</div>   		
       	</form>
      </div>
    </div>
  </div>
</div>

<!-- FIN DEL MODAL -->

<!-- Modal 2 UPDATE ##########################################################################################################-->
<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Modificar información de Institución</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="instituciones.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Ins_Institucion" id="Ins_Institucion">
					</div>
					<div class="form-group">
							<label for="Ins_Nombre"><strong>Nombre</strong></label>
							<input type="text" name="Ins_Nombre" class="form-control" id="Ins_Nombre" placeholder="Nombre"
							minlength="2" maxlength="50" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					</div>
					<div class="form-group">
						<label for="Ins_Email"><strong>Correo electrónico</strong></label>
						<input type="email" name="Ins_Email" class="form-control" id="Ins_Email" placeholder="Correo electrónico" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" title="Ingresa la dirección de correo electrónico completa.">
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="Ins_Direccion"><strong>Dirección</strong></label>
							<input type="text" name="Ins_Direccion" class="form-control" id="Ins_Direccion" placeholder="Dirección" minlength="5" maxlength="50" required>
						</div>
						<div class="form-group col-md-6">
							<label for="Ins_NoTelefono"><strong>Número de teléfono</strong></label>
							<input type="text" name="Ins_NoTelefono" class="form-control" id="Ins_NoTelefono" placeholder="Número de teléfono" minlength="8" maxlength="8" required pattern="^\d*$" title="Solo se aceptan números.">
						</div>
					</div>
					<div class="form-group">
					    <label><strong>Descripción de Institución</strong></label>
					    <textarea name="Ins_Descripcion" id="Ins_Descripcion" class="form-control" id="txtareaDescripcion" rows="3" placeholder="Descripción" minlength="5" maxlength="255"></textarea>
					</div>
				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="ins_update" id="ins_update" class="btn btn-info" value="Actualizar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 2 UPDATE ######################################################################################################-->

<!-- Modal 3 DELETE ##########################################################################################################-->
<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Eliminar información de institución</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="instituciones.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Ins_Institucion_del" id="Ins_Institucion_del">
					</div>
					
					<h5>¿Deseas eliminar este dato?</h5>

				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="ins_delete" id="ins_delete" class="btn btn-info" value="Eliminar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 3 DELETE ######################################################################################################-->

<div>
	<div class="header">
		<h1>Instituciones</h1>
		<p>
			<button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#exampleModal">
  				+ Ingresar nuevo
			</button>
		</p>
	</div>
	<div class="container_donacion">
		
		<?php 
			$query_run = mysqli_query($con, "SELECT * FROM afa_institucion AFAIS WHERE AFAIS.Ins_EstadoData = 'Y'");
		?>
		<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">Nombre</th>
		      <th scope="col">Correo Electrónico</th>
		      <th scope="col">Dirección</th>
		      <th scope="col">Teléfono</th>
		      <th scope="col">Fecha de registro</th>
		      <th scope="col">Descripción</th>
		      <th scope="col">Opciones</th>
		      <th></th>
		    </tr>
		  </thead>
		<?php
			if ($query_run) {
				foreach ($query_run as $row){
		?>
		  <tbody>
		    <tr>
		    	<td> <?php echo $row['Ins_Institucion'];?></td>
			    <td> <?php echo $row['Ins_Nombre'];?> </td>
			    <td> <?php echo $row['Ins_Email'];?> </td>
			    <td> <?php echo $row['Ins_Direccion'];?> </td>
			    <td> <?php echo $row['Ins_NoTelefono'];?> </td>
			    <td> <?php echo $row['Ins_FechaRegistro'];?> </td>
			    <td> <?php echo $row['Ins_Descripcion'];?> </td>
			    <td>
			    	<button type="button" class="btn btn-outline-info editbtn">Modificar</button>		    
			    	<button type="button" class="btn btn-outline-danger deletebtn" style="margin-top: 2%;">Eliminar</button>
			    </td>
		    </tr>
		  </tbody>
		<?php
				}
			}else{
				echo "No se encontraron registros";
			}
		?>
		</table>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<!-- SCRIPT PARA VER EL MODAL DE DELETE -->
<script>
	
	$(document).ready(function (){
		$('.deletebtn').on('click', function() {
			$('#deletemodal').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();

			console.log(data[0]);

			$('#Ins_Institucion_del').val(data[0]);
		});
	});
</script>

<!-- SCRIPT PARA VER EL MODAL DE UPDATE -->
<script>
	
	$(document).ready(function (){
		$('.editbtn').on('click', function() {
			$('#editmodal').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();


			$('#Ins_Institucion').val(data[0]);
			$('#Ins_Nombre').val(data[1].trim());
			$('#Ins_Email').val(data[2].trim());
			$('#Ins_Direccion').val(data[3].trim());
			$('#Ins_NoTelefono').val(data[4].trim());
			$('#Ins_Descripcion').val(data[6].trim());
		});
	});


</script>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>

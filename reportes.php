<?php
include("includes/header.php");
//session_destroy();;
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">

<div>
	<div class="header">
		<h1>Reportes</h1>
	</div>
	<div class="container_donacion">
		<table class="table table-hover display" id="tabla1" style="width:100%">
		  <thead>
		    <tr>
		      <th scope="col" class="text-center">ID</th>
		      <th scope="col" class="text-center">Módulo</th>
		      <th scope="col" class="text-center">Descripción</th>
		      <th scope="col" class="text-center">Opción</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      	<td class="text-center"> 1 </td>
			    <td class="text-center"> Productos </td>
			    <td class="text-center"> Inventario actual </td>
			    <td class="text-center">
			    	<a href="reportes/producto/reporte-inv.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 2 </td>
			    <td class="text-center"> Productos </td>
			    <td class="text-center"> Inventario actual de víveres </td>
			    <td class="text-center">
			    	<a href="reportes/producto/reporte-inv-v.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 3 </td>
			    <td class="text-center"> Productos </td>
			    <td class="text-center"> Inventario actual de prendas </td>
			    <td class="text-center">
			    	<a href="reportes/producto/reporte-inv-p.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		     <tr>
		      	<td class="text-center"> 4 </td>
			    <td class="text-center"> Suscriptores </td>
			    <td class="text-center"> Suscriptores existentes </td>
			    <td class="text-center">
			    	<a href="reportes/suscriptor/reporte-sus.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 5 </td>
			    <td class="text-center"> Suscriptores </td>
			    <td class="text-center"> Donadores existentes </td>
			    <td class="text-center">
			    	<a href="reportes/suscriptor/reporte-sus-d.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 6 </td>
			    <td class="text-center"> Suscriptores </td>
			    <td class="text-center"> Voluntarios existentes </td>
			    <td class="text-center">
			    	<a href="reportes/suscriptor/reporte-sus-v.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 7 </td>
			    <td class="text-center"> Intermediarios </td>
			    <td class="text-center"> Intermediarios existentes </td>
			    <td class="text-center">
			    	<a href="reportes/intermediario/reporte-int.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 8 </td>
			    <td class="text-center"> Familias beneficiadas </td>
			    <td class="text-center"> Familias que reciben ayuda </td>
			    <td class="text-center">
			    	<a href="reportes/familia/reporte-fam.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 9 </td>
			    <td class="text-center"> Instituciones </td>
			    <td class="text-center"> Instituciones que son parte de la comunidad </td>
			    <td class="text-center">
			    	<a href="reportes/institucion/reporte-ins.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 10 </td>
			    <td class="text-center"> Donaciones </td>
			    <td class="text-center"> Donaciones que se han recibido y entregado </td>
			    <td class="text-center">
			    	<a href="reportes/donacion/reporte-don.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 11 </td>
			    <td class="text-center"> Donaciones recibidas </td>
			    <td class="text-center"> Donaciones que se han recibido </td>
			    <td class="text-center">
			    	<a href="reportes/donacion/reporte-don-r.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 12 </td>
			    <td class="text-center"> Especies recibidas </td>
			    <td class="text-center"> Víveres y prendas que se han recibido. </td>
			    <td class="text-center">
			    	<a href="reportes/donacion/reporte-det.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 13 </td>
			    <td class="text-center"> Víveres recibidas </td>
			    <td class="text-center"> Víveres que se han recibido. </td>
			    <td class="text-center">
			    	<a href="reportes/donacion/reporte-det-vivere.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		    <tr>
		      	<td class="text-center"> 14 </td>
			    <td class="text-center"> Prendas recibidas </td>
			    <td class="text-center"> Prendas que se han recibido. </td>
			    <td class="text-center">
			    	<a href="reportes/donacion/reporte-det-prenda.php" target="_blank" class="btn btn-outline-info">Obtener</a>
			    </td>
		    </tr>
		  </tbody>
		</table>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<script>
		$(document).ready(function() {
    		$('#tabla1').DataTable();
		} );
</script>



</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>

<?php
include("includes/header.php");
//$con = mysqli_connect("localhost", "root", "Z7haOwSi5bMc", "afadb");
//session_destroy();
require 'includes/familia/familia_insert.php';
require 'includes/familia/familia_update.php';
require 'includes/familia/familia_delete.php';
?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Ingresar información de familia beneficiada</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       	<form action="familias.php" method="POST">
       		<div class="form-group">
					<label for="fam_apellidos"><strong>Apellidos</strong></label>
					<input type="text" name="fam_apellidos_in" class="form-control" id="fam_apellidos_in" placeholder="Apellidos"
					minlength="5" maxlength="50" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
			</div>
			<div class="form-group">
				<label for="fam_direccion"><strong>Dirección</strong></label>
				<input type="text" name="fam_direccion_in" class="form-control" id="fam_direccion_in" placeholder="Dirección" minlength="5" maxlength="50" required>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="fam_telefono"><strong>Número de teléfono</strong></label>
					<input type="text" name="fam_telefono_in" class="form-control" id="fam_telefono_in" placeholder="Dirección" minlength="1" maxlength="8" required pattern="^\d*$" title="Solo se aceptan números.">
				</div>
				<div class="form-group col-md-6">
					<label for="fam_integrantes"><strong>Número de integrantes</strong></label>
					<input type="text" name="fam_integrantes_in" class="form-control" id="fam_integrantes_in" placeholder="Número de integrantes" required pattern="^\d*$" title="Solo se aceptan números.">
				</div>
			</div>
		  	<div class="row">
				<div class="col text-center">
					<input type="submit" name="fam_button" class="btn btn-info" value="Guardar">
					<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>

				</div>
			</div>
       	</form>
      </div>
    </div>
  </div>
</div>
<!-- FIN DEL MODAL -->

<!-- Modal 2 UPDATE ##########################################################################################################-->
<div class="modal fade" id="editmodal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Modificar información de Familia</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="familias.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Fam_Familia" id="Fam_Familia">
					</div>
					<div class="form-group">
						<label for="fam_apellidos"><strong>Apellidos</strong></label>
						<input type="text" name="fam_apellidos" class="form-control" id="fam_apellidos" placeholder="Apellidos"
						minlength="5" maxlength="50" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					</div>
					<div class="form-group">
						<label for="fam_direccion"><strong>Dirección</strong></label>
						<input type="text" name="fam_direccion" class="form-control" id="fam_direccion" placeholder="Dirección" minlength="5" maxlength="50" required>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="fam_telefono"><strong>Número de teléfono</strong></label>
							<input type="text" name="fam_telefono" class="form-control" id="fam_telefono" placeholder="Dirección" minlength="8" maxlength="8" required pattern="^\d*$" title="Solo se aceptan números.">
						</div>
						<div class="form-group col-md-6">
							<label for="fam_integrantes"><strong>Número de integrantes</strong></label>
							<input type="text" name="fam_integrantes" class="form-control" id="fam_integrantes" placeholder="Número de integrantes" required pattern="^\d*$" title="Solo se aceptan números.">
						</div>
					</div>
				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="fam_update" id="fam_update" class="btn btn-info" value="Actualizar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 2 UPDATE ######################################################################################################-->

<!-- Modal 3 DELETE ##########################################################################################################-->
<div class="modal fade" id="deletemodal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Eliminar información de familia</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="familias.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Fam_Familia_del" id="Fam_Familia_del">
					</div>
					
					<h5>¿Deseas eliminar este dato?</h5>

				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="fam_delete" id="fam_delete" class="btn btn-info" value="Eliminar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 3 DELETE ######################################################################################################-->

<div>
	<div class="header">
		<h1>Familias</h1>
		<p>
			<button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#exampleModal">
  				+ Ingresar nueva
			</button>
		</p>
	</div>
	<div class="container_donacion">

		<?php 
			$query_run = mysqli_query($con, "SELECT * FROM afa_familia WHERE Fam_EstadoData = 'Y'");
		?>
		<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">Apellidos</th>
		      <th scope="col">Número de integrantes</th>
		      <th scope="col">Teléfono</th>
		      <th scope="col">Dirección</th>
		      <th scope="col">Fecha de registro</th>
		      <th></th>
		    </tr>
		  </thead>
		  <?php
			if ($query_run) {
				foreach ($query_run as $row){
		?>
		  <tbody>
		    <tr>
		      	<td> <?php echo $row['Fam_Familia'];?></td>
			    <td> <?php echo $row['Fam_Apellidos'];?> </td>
			    <td class="text-center"> <?php echo $row['Fam_NoIntegrantes'];?> </td>
			    <td> <?php echo $row['Fam_NoTelefono'];?> </td>
			    <td> <?php echo $row['Fam_Direccion'];?> </td>
			    <td> <?php echo $row['Fam_FechaRegistro'];?> </td>
			    <td>
			    	<button type="button" class="btn btn-outline-info editbtn2">Modificar</button>
			    	<button type="button" class="btn btn-outline-danger deletebtn2">Eliminar</button>
			    	<a class="btn btn-outline-secondary" style="text-decoration:none;" href="<?php print "familia_recibido.php?id=".$row['Fam_Familia']; ?>">Donaciones recibidas</a>
			    </td>
		    </tr>
		  </tbody>
		<?php
				}
			}else{
				echo "No se encontraron registros";
			}
		?>
		</table>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<!-- SCRIPT PARA VER EL MODAL DE DELETE -->
<script>
	
	$(document).ready(function (){
		$('.deletebtn2').on('click', function() {
			$('#deletemodal2').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();

			console.log(data[0]);

			$('#Fam_Familia_del').val(data[0]);
		});
	});
</script>

<!-- SCRIPT PARA VER EL MODAL DE UPDATE -->
<script>
	
	$(document).ready(function (){
		$('.editbtn2').on('click', function() {
			$('#editmodal2').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();


			$('#Fam_Familia').val(data[0]);
			$('#fam_apellidos').val(data[1].trim());
			$('#fam_integrantes').val(data[2].trim());
			$('#fam_telefono').val(data[3].trim());
			$('#fam_direccion').val(data[4].trim());
		});
	});
</script>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>

<?php
include("includes/header.php");
//session_destroy();
$id = isset($_GET['id'])?$_GET['id']:0;
?>

<link rel="stylesheet" type="text/css" href="css/producto_style.css">
<script src="js/producto.js"></script>

<div class="wrapper">
	<div id="contenedor" class="form_box1">
		<div class="card-body">
			<div>
			    <!--form action="donacion_ent_consulta.php" method="POST"-->
			       	
					      <h3><strong>Donación</strong></h3>  
					      <?php
					        $sql = "SELECT AFAD.Don_Donacion, AFAD.Don_FechaRecibido, AFAD.Don_FechaEntrega, AFAD.Us_UsuarioRecibe, AFAD.Us_UsuarioEntrega, 
								AFAI.Int_Nombre, AFAS.Sus_Nombre, AFAS.Sus_Apellido, AFAF.Fam_Apellidos, AFAD.Don_Estado FROM afa_donacion AFAD 
								INNER JOIN afa_intermediario AFAI
								ON AFAI.Int_Intermediario = AFAD.Int_Intermediario
								INNER JOIN afa_suscriptor AFAS
								ON AFAS.Sus_Suscriptor = AFAD.Sus_Suscriptor
								INNER JOIN afa_familia AFAF
								ON AFAF.Fam_Familia = AFAD.Fam_Familia
								WHERE AFAD.Don_EstadoData = 'Y'
								AND AFAD.Don_Estado = 'Entregada' AND AFAD.Don_Donacion={$id}";
					        $resultset = mysqli_query($con, $sql) or die("database error:". mysqli_error($con));
					        while( $record = mysqli_fetch_assoc($resultset) ) {
					      ?>
					            <!--div>
					              <h5 class="mt-0 mb-1"><?php echo $record['Us_UsuarioRecibe']; ?></h5>
					              <p><?php echo $record['Us_UsuarioEntrega']; ?></p>
					              <p><strong>Correo electrónico</strong>: <?php echo $record['Don_FechaRecibido']; ?></p>
					              <p><strong>Teléfono</strong>: <?php echo $record['Don_FechaEntrega']; ?></p>
					              <p><strong>Dirección</strong>: <?php echo $record['Int_Nombre']; ?></p>
					            </div-->

					            <div class="card" style="width: 25rem;">
							        <div class="card-body">
							            <dl class="row">
							                <dt class="col-sm-7">
							                    <label>Usuario que recibe</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Us_UsuarioRecibe']; ?>
							                </dd>
							                <dt class="col-sm-7">
							                    <label>Usuario que entrega</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Us_UsuarioEntrega']; ?>
							                </dd>
							                <dt class="col-sm-7">
							                    <label>Fecha que recibe</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Don_FechaRecibido']; ?>
							                </dd>
							                <dt class="col-sm-7">
							                    <label>Fecha que entrega</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Don_FechaEntrega']; ?>
							                </dd>
							               	<dt class="col-sm-7">
							                    <label>Intermediario</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Int_Nombre']; ?>
							                </dd>
							                <dt class="col-sm-7">
							                    <label>Donador</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Sus_Nombre'].' '.$record['Sus_Apellido']; ?>
							                </dd>
							                <dt class="col-sm-7">
							                    <label>Familia que recibe</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Fam_Apellidos']; ?>
							                </dd>
							                <dt class="col-sm-7">
							                    <label>Estado de donación</label>
							                </dt>
							                <dd class="col-sm-5">
							                    <?php echo $record['Don_Estado']; ?>
							                </dd>							               
							            </dl>


							            <div class="container_donacion">
							            	<h5>Detalle de donación</h5>
											<?php 
												$query_run = mysqli_query($con, "SELECT AFADD.DDR_Cantidad, AFAP.Pro_Nombre, AFADD.DDR_FechaVencimiento FROM afa_detalle_donacionrecibida AFADD
													INNER JOIN afa_donacion AFAD
													ON AFADD.Don_Donacion = AFAD.Don_Donacion
													INNER JOIN afa_producto AFAP
													ON AFADD.DDR_Producto = AFAP.Pro_Producto
													WHERE AFADD.Don_Donacion = {$id};");
											?>
											<table class="table table-hover">
											  <thead>
											    <tr>
											      <th scope="col">Nombre</th>
											      <th scope="col">Fecha de vencimiento</th>
											      <th scope="col">Cantidad</th>
											    </tr>
											  </thead>
											  <?php
												if ($query_run) {
													foreach ($query_run as $row){
											?>
											  <tbody>
											    <tr>
											      	<td> <?php echo $row['Pro_Nombre'];?></td>
												    <td> <?php echo $row['DDR_FechaVencimiento']?$row['DDR_FechaVencimiento']:"N/A";?> </td>
												    <td class="text-center"> <?php echo $row['DDR_Cantidad'];?> </td>
											    </tr>
											  </tbody>
											<?php
													}
												}else{
													echo "No se encontraron registros";
												}
											?>
											</table>
										</div>


							            <form style="text-align: center;">
							                <a href="donacion_entregada.php" class="btn btn-outline-dark">Volver <a/> 
							            </form>
							        </div>
							    </div>

					      <?php } ?>			
			  	</form>
			</div>
		</div>
	</div>
</div>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>

<!--div class="card" style="width: 25rem;">
        <div class="card-body" style="width: 1000px;">
            <h4 class="card-title">Ubicación</h4>
            <br />
            <dl class="row">
                <dt class="col-sm-2">
                    @Html.DisplayNameFor(model => model.Descripcion)
                </dt>
                <dd class="col-sm-10">
                    @Html.DisplayFor(model => model.Descripcion)
                </dd>
                <dt class="col-sm-2">
                    @Html.DisplayNameFor(model => model.Direccion)
                </dt>
                <dd class="col-sm-10">
                    @Html.DisplayFor(model => model.Direccion)
                </dd>
                <dt class="col-sm-2">
                    @Html.DisplayNameFor(model => model.EstadoData)
                </dt>
                <dd class="col-sm-10">
                    @Html.DisplayFor(model => model.EstadoData)
                </dd>
            </dl>

            <form asp-action="Delete">
                <input type="hidden" asp-for="IdUbicacion" />
                <input type="submit" value="Eliminar" class="btn btn-danger" /> |
                <a asp-action="Index">Volver a la lista</a>
            </form>
        </div>
    </div-->


<?php
include("includes/header.php");
require 'includes/donacion/donacion_insert.php';
?>

<div class="wrapper">
	<div id="contenedor" class="form_box">
		<div class="modal-header">
	        <h4 class="modal-title" id="exampleModalLabel">Ingresar donación</h4>
      	</div>
		<div class="modal-body">
			<div id="first">
			    <form action="donacion_ingreso.php" method="POST">
			       	<div class="form-group">
						<label for="inputNombre"><strong>Usuario que registra</strong></label>
						<input type="text" name="don_usuario_recibe" class="form-control" id="don_usuario_recibe" 
						value="<?php echo $user['Us_Nombre'] . " " . $user['Us_Apellido']; ?>">
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
						   	<label><strong>Intermediario</strong></label>
							<select class="form-control" id="don_intermediario" name="don_intermediario">
			        			<option value="0">Seleccione:</option>
			        			<?php $query = mysqli_query($con, "SELECT * FROM afa_intermediario");
			          			while ($valores = mysqli_fetch_array($query)) { 
			          			echo '<option value="'.$valores[Int_Intermediario].'">'.$valores[Int_Nombre].'</option>'; } ?>
			          		</select>	
						</div>
						<div class="form-group col-md-6">
						   	<label><strong>Suscriptor</strong></label>
							<select class="form-control" id="don_suscriptor" name="don_suscriptor">
						        <option value="0">Seleccione:</option>
						        <?php $query = mysqli_query($con, "SELECT * FROM afa_suscriptor");
						         while ($valores1 = mysqli_fetch_array($query)) { 
						         echo '<option value="'.$valores1[Sus_Suscriptor].'">'.$valores1[Sus_Nombre].' '.$valores1[Sus_Apellido].'</option>'; } ?>
						    </select>
						</div>
		  			</div>
					<div class="row">
						<div class="col text-center">
							<input type="submit" name="don_button" id="don_button" class="btn btn-success" value="+ Producto">
							<a href="donacion.php" class="btn btn-dark">Volver</a>
							<br>							
						</div>
					</div> 		
			  	</form>
			</div>
		</div>
	</div>
</div>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>

<?php
include("includes/header.php");
//session_destroy();
require 'includes/donacion/donacion_delete.php';
require 'includes/donacion_entregada/donacion_ent_insert.php';
?>

<!-- Modal 3 DELETE ##########################################################################################################-->
<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Eliminar información de donación</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="donacion.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Don_Donacion_del" id="Don_Donacion_del">
					</div>
					
					<h5>¿Deseas eliminar este dato?</h5>

				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="don_delete" id="don_delete" class="btn btn-info" value="Eliminar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 3 DELETE ######################################################################################################-->

<!-- Modal 2 DELIVER ##########################################################################################################-->
<div class="modal fade" id="delivermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Entregar donación</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="donacion.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Don_Donacion" id="Don_Donacion">
					</div>
					<div class="form-group">
						<label for="inputNombre"><strong>Usuario que entrega</strong></label>
						<input type="text" name="don_usuario_entrega" class="form-control" id="don_usuario_entrega" 
						value="<?php echo $user['Us_Nombre'] . " " . $user['Us_Apellido']; ?>">
					</div>
					<div class="form-group">
					<label><strong>Familia que recibe</strong></label>
						<select class="form-control" id="familia_rec" name="familia_rec" required>
			        		<option value="0">Seleccione:</option>
			        			<?php $query = mysqli_query($con, "SELECT * FROM afa_familia WHERE Fam_EstadoData='Y'");
			          				while ($valores = mysqli_fetch_array($query)) { 
			          					echo '<option value="'.$valores[Fam_Familia].'">'.$valores[Fam_Apellidos].'</option>'; } ?>
			          	</select>
					</div>			
				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="btn_deliver" id="btn_deliver" class="btn btn-info" value="Guardar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal DELIVER ######################################################################################################-->


<div>
	<div class="header">
		<h1>Donaciones recibidas</h1>
		<p>
			<a href="donacion_ingreso.php" class="btn btn-outline-dark btn-sm">+ Agregar nuevo</a>
		</p>
		</p>
	</div>
	<div class="container_donacion">
		<?php 
			$query_run = mysqli_query($con, "SELECT AFAD.Don_Donacion, AFAD.Don_FechaRecibido, AFAD.Us_UsuarioRecibe, AFAI.Int_Nombre,
AFAS.Sus_Nombre, AFAS.Sus_Apellido, AFAD.Don_Estado FROM afa_donacion AFAD 
INNER JOIN afa_intermediario AFAI
ON AFAI.Int_Intermediario = AFAD.Int_Intermediario
INNER JOIN afa_suscriptor AFAS
ON AFAS.Sus_Suscriptor = AFAD.Sus_Suscriptor
WHERE AFAD.Don_EstadoData = 'Y' AND AFAD.Don_Estado = 'Recibida'
ORDER BY AFAD.Don_Donacion
"); 
?>
	<table class="table table-hover">
		<thead>
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">Fecha que recibe</th>
		      <th scope="col">Usuario que recibe</th>
		      <th scope="col">Intermediario</th>
		      <th scope="col">Nombre Donador</th>
		      <th scope="col">Apellido Donador</th>
		      <th scope="col">Estado</th>
		      <th scope="col">Opciones </th>
		    </tr>
		  </thead>
		  <?php
			if ($query_run) {
				foreach ($query_run as $row){
		?>
		  <tbody>
		    <tr>
		      <td> <?php echo $row['Don_Donacion'];?></td>
			    <td> <?php echo $row['Don_FechaRecibido'];?> </td>
			    <td> <?php echo $row['Us_UsuarioRecibe'];?> </td>
			    <td> <?php echo $row['Int_Nombre'];?> </td>
			    <td> <?php echo $row['Sus_Nombre'];?> </td>
			    <td> <?php echo $row['Sus_Apellido'];?> </td>
			    <td> <?php echo $row['Don_Estado'];?> </td>
			    <td>
			    	<a class="btn btn-outline-info editbtn" style="text-decoration:none;" href="<?php print "detalle.php?id=".$row['Don_Donacion']; ?>">Ver Detalle</a>
			    	<button type="button" class="btn btn-outline-danger deletebtn">Eliminar</button>
			    	<button type="button" class="btn btn-outline-success deliverbtn">Entregar</button>
			    </td>
		    </tr>
		  </tbody>
		  <?php
				}
			}else{
				echo "No se encontraron registros";
			}
		?>
		</table>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>

<!-- SCRIPT PARA VER EL MODAL DE DELETE -->
<script>
	
	$(document).ready(function (){
		$('.deletebtn').on('click', function() {
			$('#deletemodal').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();

			console.log(data[0]);

			$('#Don_Donacion_del').val(data[0]);
		});
	});
</script>

<!-- SCRIPT PARA VER EL MODAL DE DELIVER -->
<script>
	
	$(document).ready(function (){
		$('.deliverbtn').on('click', function() {
			$('#delivermodal').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();


			$('#Don_Donacion').val(data[0]);
			//$('#fam_apellidos').val(data[1]);
			//$('#fam_integrantes').val(data[2]);
			//$('#fam_telefono').val(data[3]);
			//$('#fam_direccion').val(data[4]);
		});
	});
</script>

<script>
	function detalle(){
		
	}
</script>
</html>

<?php
include("includes/header.php");
//session_destroy();
require 'includes/form_handlers/suscriptor_handler.php';
require 'includes/suscriptor/suscriptor_update.php';
require 'includes/suscriptor/suscriptor_delete.php';
?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Ingresar información de suscriptor</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form method="POST" action="suscriptor.php">
		       		<div class="form-row">
					    <div class="form-group col-md-6">
					      <label for="inputNombre"><strong>Nombre</strong></label>
					      <input type="text" class="form-control" name="inputNombre" id="inputNombre" placeholder="Nombre" value=""  minlength="2" maxlength="25" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					    </div>
					    <div class="form-group col-md-6">
					      <label for="inputApellido"><strong>Apellido</strong></label>
					      <input type="text" class="form-control" name="inputApellido" id="inputApellido" placeholder="Apellido" value="" minlength="2" maxlength="25" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					    </div>
					</div>
					<div class="form-group">
					    <label for="inputEmail"><strong>Correo electrónico</strong></label>
					    <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Correo electrónico" value="<?php 
					if (isset($_SESSION['sus_email'])) {
						echo $_SESSION['sus_email'];
					} ?>" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}">
				<?php if (in_array("Esta dirección de correo electrónico ya está registrada<br>", $error_array)) echo "Esta dirección de correo electrónico ya está registrada<br>";
						else if (in_array("Formato inválido<br>", $error_array)) echo "Formato inválido<br>"; ?>
					</div>
					<div class="form-group">
						<div class="row">
						   	<label><strong>Tipo de suscripcion</strong></label>
						    <div class="col-sm-10">
						        <div class="form-check">
						          <input class="form-check-input" type="radio" name="tipoSuscripcion" id="gridRadios1" value="Donador" required>
						          <span class="w-form-label">Donador</span>
						        </div>
						        <div class="form-check">
						          <input class="form-check-input" type="radio" name="tipoSuscripcion" id="gridRadios2" value="Voluntario" required>
						          <span class="w-form-label">Voluntario</span>
						    	</div>
						    </div>
						</div>
		  			</div>
		  			<div class="row">
						<div class="col text-center">
						  
						   	 <button name="sus1_button" type="submit" class="btn btn-info">Guardar</button>
						   	
						   
						    <button data-dismiss="modal" type="submit" class="btn btn-danger">Cancelar</button>
						 </div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Fin de Modal -->


<!-- Modal 2 UPDATE ##########################################################################################################-->

<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Modificar información de suscriptor</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form method="POST" action="suscriptor.php">
		       		<div class="form-row">
		       			<input type="hidden" name="Sus_Suscriptor" id="Sus_Suscriptor">
					    <div class="form-group col-md-6">
					      <label for="Sus_Nombre"><strong>Nombre</strong></label>
					      <input type="text" class="form-control" name="Sus_Nombre" id="Sus_Nombre" placeholder="Nombre" value=""  minlength="2" maxlength="25" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					    </div>
					    <div class="form-group col-md-6">
					      <label for="Sus_Apellido"><strong>Apellido</strong></label>
					      <input type="text" class="form-control" name="Sus_Apellido" id="Sus_Apellido" placeholder="Apellido" value="" minlength="2" maxlength="25" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					    </div>
					</div>
					<div class="form-group">
					    <label for="Sus_Email"><strong>Correo electrónico</strong></label>
					    <input type="email" class="form-control" name="Sus_Email" id="Sus_Email" placeholder="Correo electrónico" value="<?php 
					if (isset($_SESSION['Sus_Email'])) {
						echo $_SESSION['Sus_Email'];
					} ?>" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" title="Ingresa la dirección de correo electrónico completa.">
				<?php if (in_array("Esta dirección de correo electrónico ya está registrada<br>", $error_array)) echo "Esta dirección de correo electrónico ya está registrada<br>";
						else if (in_array("Formato inválido<br>", $error_array)) echo "Formato inválido<br>"; ?>
					</div>
					<div class="form-group">
						<div class="row">						  	
						    <div class="col-sm-10" id="cnt_Sus_TipoSuscripcion">
						        <label><strong>Tipo de suscripcion</strong></label>
						    	<select class="form-control" id="Sus_TipoSuscripcion" name="Sus_TipoSuscripcion" required>
							      <option value="Donador">Donador</option>
							      <option value="Voluntario">Voluntario</option>
							    </select>

						    </div>
						</div>
		  			</div>
		  			<div class="row">
						<div class="col text-center">						  
						   	<button name="update_data" type="submit" class="btn btn-info">Actualizar</button>						 					   
						    <button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						 </div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 2 UPDATE ######################################################################################################-->

<!-- Modal 3 DELETE ##########################################################################################################-->

<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Eliminar información de suscriptor</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>

	      	<form method="POST" action="suscriptor.php">
		       	<div class="modal-body">
		       		<div class="form-row">
		       			<input type="hidden" name="Sus_Suscriptor_del" id="Sus_Suscriptor_del">
					    <h5>¿Quieres borrar estos datos?</h5>
					</div>
				</div>
				<div class="modal-footer">
		        	<button name="delete_data" type="submit" class="btn btn-danger">Sí</button>
					<button type="button" data-dismiss="modal" class="btn btn-info">No</button>
		     </div>
			</form>					
		</div>
	</div>
</div>
<!-- FIN DEL Modal 3 DELETE ######################################################################################################-->

<div>
	<div class="header">
		<h1>Suscriptores</h1>
		<p>
			<button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#exampleModal">
  				+ Ingresar nuevo
			</button>
		</p>
	</div>
	<div class="container_donacion">

		<?php 
			$query_run = mysqli_query($con, "SELECT * FROM afa_suscriptor AFAS WHERE AFAS.Sus_EstadoData = 'Y'");
		?>
		<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">ID</th> 	
		      <th scope="col">Nombre</th>
		      <th scope="col">Apellido</th>
		      <th scope="col">Correo electrónico</th>
		      <th scope="col">Tipo de suscripción</th>
		      <th scope="col">Fecha de registro</th>
		      <th></th>
		    </tr>
		  </thead>
		<?php
			if ($query_run) {
				foreach ($query_run as $row){
		?>
		  <tbody>
		    <tr>
		    	<td> <?php echo $row['Sus_Suscriptor'];?></td>
			    <td> <?php echo $row['Sus_Nombre'];?> </td>
			    <td> <?php echo $row['Sus_Apellido'];?> </td>
			    <td> <?php echo $row['Sus_Email'];?> </td>
			    <td> <?php echo $row['Sus_TipoSuscripcion'];?> </td>
			    <td> <?php echo $row['Sus_FechaRegistro'];?> </td>
			    <td>
			    	<button type="button" class="btn btn-outline-info editbtn">Modificar</button>
			    	<button type="button" class="btn btn-outline-danger deletebtn">Eliminar</button>
			    </td>
		    </tr>		  
		  </tbody>
		<?php
				}
			}else{
				echo "No se encontraron registros";
			}
		?>
		</table>
	</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<!-- SCRIPT PARA VER EL MODAL DE DELETE -->
<script>
	
	$(document).ready(function (){
		$('.deletebtn').on('click', function() {
			$('#deletemodal').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();

			console.log(data);

			$('#Sus_Suscriptor_del').val(data[0]);
		});
	});
</script>

<!-- SCRIPT PARA VER EL MODAL DE UPDATE -->
<script>
	
	$(document).ready(function (){
		$('.editbtn').on('click', function() {
			$('#editmodal').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();

			console.log(data);

			$('#Sus_Suscriptor').val(data[0]);
			$('#Sus_Nombre').val(data[1].trim());
			$('#Sus_Apellido').val(data[2].trim());
			$('#Sus_Email').val(data[3].trim());
			$('#Sus_TipoSuscripcion').val(data[4].trim());
		});
	});
</script>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>

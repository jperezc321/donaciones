<?php
include("includes/header.php");
//session_destroy();
require 'includes/intermediario/intermediario_insert.php';
require 'includes/intermediario/intermediario_update.php';
require 'includes/intermediario/intermediario_delete.php';
?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Ingresar información de intermediario</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       	<form action="intermediario.php" method="POST">
  
			<div class="form-group">
					<label for="inputNombre"><strong>Nombre</strong></label>
					<input type="text" name="int_nombre" class="form-control" id="inputNombre" placeholder="Nombre"
					minlength="2" maxlength="50" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
			</div>
			<div class="form-group">
				<label for="inputEmail"><strong>Correo electrónico</strong></label>
				<input type="email" name="int_email" class="form-control" id="inputEmail" placeholder="Correo electrónico" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" title="Ingresa la dirección de correo electrónico completa.">
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputDireccion"><strong>Dirección</strong></label>
					<input type="text" name="int_direccion" class="form-control" id="inputDireccion" placeholder="Dirección" minlength="5" maxlength="50" required>
				</div>
				<div class="form-group col-md-6">
					<label for="inputTel"><strong>Número de teléfono</strong></label>
					<input type="text" name="int_telefono" class="form-control" id="inputTel" placeholder="Número de teléfono" minlength="8" maxlength="8" required pattern="^\d*$" title="Solo se aceptan números.">
				</div>
			</div>
			<div class="form-group">
			    <label><strong>Descripción de Intermediario</strong></label>
			    <textarea name="int_descripcion" class="form-control" id="txtareaDescripcion" rows="3" placeholder="Descripción" minlength="5" maxlength="255"></textarea>
			</div>
		  	<div class="row">
				<div class="col text-center">
					<input type="submit" name="int_button" id="int_button" class="btn btn-info" value="Guardar">
					<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
				</div>
			</div>   		
       	</form>
      </div>
    </div>
  </div>
</div>

<!-- FIN DEL MODAL -->

<!-- Modal 2 UPDATE ##########################################################################################################-->
<div class="modal fade" id="editmodal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Modificar información de intermediario</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="intermediario.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Int_Intermediario" id="Int_Intermediario">
					</div>
					<div class="form-group">
							<label for="Int_Nombre"><strong>Nombre</strong></label>
							<input type="text" name="Int_Nombre" class="form-control" id="Int_Nombre" placeholder="Nombre"
							minlength="2" maxlength="50" required pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" title="No se aceptan carácteres numéricos ni carácteres especiales.">
					</div>
					<div class="form-group">
						<label for="Int_Email"><strong>Correo electrónico</strong></label>
						<input type="email" name="Int_Email" class="form-control" id="Int_Email" placeholder="Correo electrónico" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" title="Ingresa la dirección de correo electrónico completa.">
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="Int_Direccion"><strong>Dirección</strong></label>
							<input type="text" name="Int_Direccion" class="form-control" id="Int_Direccion" placeholder="Dirección" minlength="5" maxlength="50" required>
						</div>
						<div class="form-group col-md-6">
							<label for="Int_NoTelefono"><strong>Número de teléfono</strong></label>
							<input type="text" name="Int_NoTelefono" class="form-control" id="Int_NoTelefono" placeholder="Número de teléfono" minlength="8" maxlength="8" required pattern="^\d*$" title="Solo se aceptan números.">
						</div>
					</div>
				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="int_update" id="int_update" class="btn btn-info" value="Actualizar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 2 UPDATE ######################################################################################################-->

<!-- Modal 3 DELETE ##########################################################################################################-->
<div class="modal fade" id="deletemodal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h4 class="modal-title" id="exampleModalLabel">Eliminar información de intermediario</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	      	</div>
	      	<div class="modal-body">
		       	<form action="intermediario.php" method="POST">  
					<div class="form-group">
							<input type="hidden" name="Int_Intermediario_del" id="Int_Intermediario_del">
					</div>
					
					<h5>¿Deseas eliminar este dato?</h5>

				  	<div class="row">
						<div class="col text-center">
							<input type="submit" name="int_delete" id="int_delete" class="btn btn-info" value="Eliminar">
							<button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
						</div>
					</div>   		
		       	</form>
			</div>
		</div>
	</div>
</div>
<!-- FIN DEL Modal 3 DELETE ######################################################################################################-->

<div>
	<div class="header">
		<h1>Intermediarios</h1>
		<p>
			<button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#exampleModal">
  				+ Ingresar nuevo
			</button>
		</p>
	</div>

	<div class="container_donacion">
		
		<?php 
			$query_run = mysqli_query($con, "SELECT * FROM afa_intermediario AFAI WHERE AFAI.Int_EstadoData = 'Y'");
		?>
		<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">ID</th>
		      <th scope="col">Nombre</th>
		      <th scope="col">Correo Electrónico</th>
		      <th scope="col">Dirección</th>
		      <th scope="col">Teléfono</th>
		      <th scope="col">Fecha de registro</th>
		      <th></th>
		    </tr>
		  </thead>
		<?php
			if ($query_run) {
				foreach ($query_run as $row){
		?>
		  <tbody>
		    <tr>
		    	<td> <?php echo $row['Int_Intermediario'];?></td>
			    <td> <?php echo $row['Int_Nombre'];?> </td>
			    <td> <?php echo $row['Int_Email'];?> </td>
			    <td> <?php echo $row['Int_Direccion'];?> </td>
			    <td> <?php echo $row['Int_NoTelefono'];?> </td>
			    <td> <?php echo $row['Int_FechaRegistro'];?> </td>
			    <td>
			    	<button type="button" class="btn btn-outline-info editbtn1">Modificar</button>
			    	<button type="button" class="btn btn-outline-danger deletebtn1">Eliminar</button>
			    </td>
		    </tr>
		  </tbody>
		<?php
				}
			}else{
				echo "No se encontraron registros";
			}
		?>
		</table>
	</div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>


<!-- SCRIPT PARA VER EL MODAL DE DELETE -->
<script>
	
	$(document).ready(function (){
		$('.deletebtn1').on('click', function() {
			$('#deletemodal1').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();

			console.log(data[0]);

			$('#Int_Intermediario_del').val(data[0]);
		});
	});
</script>

<!-- SCRIPT PARA VER EL MODAL DE UPDATE -->
<script>
	
	$(document).ready(function (){
		$('.editbtn1').on('click', function() {
			$('#editmodal1').modal('show'); //from bootstrap

			$tr = $(this).closest('tr');
			var data = $tr.children("td").map(function(){
				return $(this).text();
			}).get();


			$('#Int_Intermediario').val(data[0]);
			$('#Int_Nombre').val(data[1].trim());
			$('#Int_Email').val(data[2].trim());
			$('#Int_Direccion').val(data[3].trim());
			$('#Int_NoTelefono').val(data[4].trim());
		});
	});


</script>

</body>
<footer>
	<?php require_once('includes/footer.php'); ?>
</footer>
</html>

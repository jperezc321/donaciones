<?php 

	include("includes/main_header.php");

$con = mysqli_connect("localhost", "root", "Z7haOwSi5bMc", "afadb");
$con -> set_charset("utf8");
?>


  <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.css">

  <style> 
    #map {
        height: 80%;
        }
     
        html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        }

    .container{
      margin: 30px 30px 30px 100px;
      max-height: 500px;
    }
  </style>

  <div class="header-inner">
    <div class="container">
      <div class="header-inner-content">
        <h1 class="h1">Intermediarios</h1>
        <p class="paragraph" style="text-align: justify;">Nuestros intermediarios son los responsables de facilitar el proceso de donación, pues permiten a los donadores hacer sus donaciones sin tener que viajar tanto para entregarlas, pues pueden visitar el que más les favorezca, de esta forma podrán entregar su donación sin ocupar tanto tiempo. Si deseas conocer más, haz click en el siguiente botón.</p>
        <a href="#informacion" class="button-5 w-button">Ir</a>
      </div>
    </div>
  </div>
  <!--div class="premium-intro"></div-->
  <div class="facts">
    <div class="container"></div>
    <!--div class="facts-gray-background"></div-->
  </div>
  <!--div class="premium-intro"></div-->
    
  <div class="container-fluid" style="background-color: #eae6ca;">
    <div class="row" style="padding: 5% 5% 5%; max-height: 100%">
      <div class="col-sm" style="max-height: 100%; padding-top: 7%">
        <div>
          <h3><strong>¿Qué es un intermediario?</strong></h3>
        </div>
        <div>
          <p style="text-align: justify;">Los intermediarios son edificios ubicados en distintos puntos del municipio de Villa Nueva comprometidos con esta comunidad. Gracias a cada uno de ellos, nuestros donadores pueden ahorrarse tiempo al entregar sus donaciones, pues pueden acudir a la ubicación que más les favorezca. Cabe mencionar que las personas que trabajan en los mismos atraviesan un proceso antes de ser confirmados, esto para asegurarnos que son confiables.</p>
        </div>
      </div>
      <div class="col-sm" style="max-height: 100%">
        <div class="grid-block">
          <img src="images/background/intermediario-1.jpg">
        </div>
      </div>
  </div>
</div>

<div class="container-fluid" style="background-color: #FAFAFA;">
  <div class="faq1" id="informacion" style="height: 100%; width: 100%;">
    <div>
      <h3><strong>Intermediarios activos</strong></h3>  
      <?php
        $sql = "SELECT * FROM afa_intermediario WHERE Int_EstadoData='Y'";
        $resultset = mysqli_query($con, $sql) or die("database error:". mysqli_error($con));
        while( $record = mysqli_fetch_assoc($resultset) ) {
      ?>
      <ul class="list-unstyled">
        <div class="card w-90">
          <li class="media">
            <!--img class="mr-3" src="<?php echo $record['Int_Imagen']; ?>" alt="Generic placeholder image"-->
            <img class="mr-3" src="<?php echo $record['Int_Imagen']; ?>" alt="Generic placeholder image">
            <div class="media-body">
              <h5 class="mt-0 mb-1"><?php echo $record['Int_Nombre']; ?></h5>
              <p><?php echo $record['Int_Descripcion']; ?></p>
              <p><strong>Correo electrónico</strong>: <?php echo $record['Int_Email']; ?></p>
              <p><strong>Teléfono</strong>: <?php echo $record['Int_NoTelefono']; ?></p>
              <p><strong>Dirección</strong>: <?php echo $record['Int_Direccion']; ?></p>
            </div>
          </li>
        </div>
      </ul>
      <?php } ?>
    </div>
  </div>
</div>

<div class="container-fluid" style="height: 100%; width: 100%;">
  <div class="row" style="padding: 5% 5% 5%; max-height: 100%">
      <div class="col-sm" style="height: 600px;">
        <div><h3>Mapa de Ubicación de Intermediarios</h3></div>
        <div>
          <br>
          <h5>1. IDMIS</h5>
          <p><div><a href="https://goo.gl/maps/yz999Uo3QQUFBXn4A" target="_blank" class="btn btn-info">Ir con google maps</a><a href="https://goo.gl/maps/yz999Uo3QQUFBXn4A" target="_blank" class="link-block-5 w-inline-block"><img src="images/google-maps.png" loading="lazy" width="80" alt="" class="image-12"></a></div></p>
          <br>
          <h5>2. Shaddai</h5>
          <p><div><a href="https://goo.gl/maps/QtPQzQfWUpTJrUkq9" target="_blank" class="btn btn-info">Ir con google maps</a><a href="https://goo.gl/maps/QtPQzQfWUpTJrUkq9" target="_blank" class="link-block-5 w-inline-block"><img src="images/google-maps.png" loading="lazy" width="80" alt="" class="image-12"></a></div></p>  
        </div>
      </div>
      <div class="col-sm" style="height: 600px;">
        <div id="map"> </div> 
      </div>
  </div>
</div>

  <div class="footer">
    <div class="container cc-footer">
      <div class="footer-column cc-footer"><a href="index.php" class="navigation-logo w-inline-block"><img src="images/all-x-all-2.png" width="143" srcset="images/all-x-all-2-p-500.png 500w, images/all-x-all-2-p-800.png 800w, images/all-x-all-2.png 1030w" sizes="(max-width: 479px) 30vw, 143px" alt=""></a>
        <div class="text-footer-credits">© 2020 All for All Inc, All rights reserved.</div>
      </div><!--div class="premium-intro"></div-->
      <div class="footer-column">
        <div class="footer-links-list"><a href="index.php" class="link-footer">Inicio</a><a href="about.php" class="link-footer">Conócenos</a><a href="instituciones_info.php" aria-current="page" class="link-footer w--current">Instituciones</a><a href="intermediario_info.php" aria-current="page" class="link-footer w--current">Intermediarios</a></div>
        <div class="footer-social"><a href="#" class="link-social w-inline-block"><img src="images/icon-facebook.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-twitter.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-instagram.svg" alt=""></a></div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5f571a518e3cdfe11baf4271" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-yZzrX2-m4td9UFKPTLaYwiW5TzD4vxc&callback=initMap" async defer></script>
   
        <script>
          var map;
          function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 14.521214, lng: -90.586320},
            zoom: 15,
            });
            var marker = new google.maps.Marker({
            position: {lat: 14.520554, lng: -90.585890},
            map: map,
            title: 'IDMIS'
            });
            var marker1 = new google.maps.Marker({
            position: {lat: 14.518391, lng: -90.580717},
            map: map,
            title: 'Shaddai'
            });
          }
        </script>


</body>
</html>

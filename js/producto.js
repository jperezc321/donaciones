$(document).ready(function(){

	//on click REGISTRO, hide login and show registration form
	$("#registro").click(function() {
		$("#first").slideDown("slow", function(){
			$("#second").slideDown("slow");
		});
	});

	//on click LOGIN, hide registration and show login form
	$("#login").click(function() {
		$("#second").slideUp("slow", function(){
			$("#first").slideDown("slow");
		});
	});

});


<?php 

	include("includes/main_header.php");

$con = mysqli_connect("localhost", "root", "Z7haOwSi5bMc", "afadb");
$con -> set_charset("utf8");
?>


  <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.css">

  <style> 
    #map {
      height: 80%;
   }

        html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        }

    .container{
      margin: 30px 30px 30px 100px;
      max-height: 500px;
    }
  </style>

  <div class="header-inner">
    <div class="container">
      <div class="header-inner-content">
        <h1 class="h1">¿A quiénes ayudamos?</h1>
        <p class="paragraph" style="text-align: justify;">Aparte de las familias a quienes constantemente brindamos ayuda gracias a ti, nos alegra poder proveer de una plataforma a algunas instituciones aparte, cuya información de contacto podrás encontrar en este espacio si deseas contactarlos directamente.</p>
        <a href="#informacion" class="button-5 w-button">Ir</a></div>
    </div>
  </div>
  <!--div class="premium-intro"></div-->
  <div class="facts">
    <div class="container"></div>
    <!--div class="facts-gray-background"></div-->
  </div>
  <!--div class="premium-intro"></div-->

  <div class="container-fluid" style="background-color: #eae6ca;">
    <div class="row" style="padding: 5% 5% 5%; max-height: 100%">
      <div class="col-sm" style="max-height: 100%; padding-top: 7%">
        <div>
          <h3><strong>¿Qué tipo de instituciones apoyamos?</strong></h3>
        </div>
        <div>
          <p style="text-align: justify;">Como comunidad enfocada en brindar ayuda social a quienes más lo necesitan, estamos comprometidos a proveer un espacio en nuestra plataforma para dar visibilidad a diversas instituciones en Villa Nueva que albergan y/o ayudan a personas que lo necesitan, se sabe que existen muchas instituciones que aunque actualmente reciben ayuda, siempre necesitan más para poder cubrir totalmente las necesidades básicas de las personas a quienes cuidan. Por esta razón, acá podrás encontrar diversas instituciones que estarán felices y muy agradecidas de recibir tu apoyo.</p>
        </div>
      </div>
      <div class="col-sm" style="max-height: 100%">
        <div class="grid-block">
          <img src="images/background/institucion.jpg">
        </div>
      </div>
  </div>
</div>
    
    <div class="faq1" id="informacion">
      <div>
        <h1><strong>Instituciones</strong></h1>
        <?php
          $sql = "SELECT * FROM afa_institucion WHERE Ins_EstadoData='Y'";
          $resultset = mysqli_query($con, $sql) or die("database error:". mysqli_error($con));
          while( $record = mysqli_fetch_assoc($resultset) ) {
        ?>
        <ul class="list-unstyled">
          <div class="card w-90" style="width: 100%">
            <li class="media">
              <img class="mr-3" src="<?php echo $record['Ins_Imagen']; ?>" alt="Generic placeholder image">
              <div class="media-body">
                <h5 class="mt-0 mb-1"><?php echo $record['Ins_Nombre']; ?></h5>
                <p><?php echo $record['Ins_Descripcion']; ?></p>
                <p><strong>Correo electrónico</strong>: <?php echo $record['Ins_Email']; ?></p>
                <p><strong>Teléfono</strong>: <?php echo $record['Ins_NoTelefono']; ?></p>
                <p><strong>Dirección</strong>: <?php echo $record['Ins_Direccion']; ?></p>                
              </div>
            </li>
          </div>
        </ul>
        <?php } ?>
      </div>
    </div>

<div class="container-fluid" style="max-height: 100%">
  <div class="row" style="padding: 5% 5% 5% 5%; max-height: 100%">
      <div class="col-sm" style="height: 600px;">
        <div><h3>Mapa de Ubicación de Instituciones</h3></div>
        <div>
          <br>
          <h5>1. Asilo San José</h5>
          <p><div><a href="https://goo.gl/maps/yt3w599yXVWCUy6n9" target="_blank" class="btn btn-info">Ir con google maps</a><a href="https://goo.gl/maps/yt3w599yXVWCUy6n9" target="_blank" class="link-block-5 w-inline-block"><img src="images/google-maps.png" loading="lazy" width="80" alt="" class="image-12"></a></div></p>
          <br>
          <h5>2. Orfanato Liga de Vida Nueva</h5>
          <p><div><a href="https://goo.gl/maps/9wk1Xm6rbmACkeMJ6" target="_blank" class="btn btn-info">Ir con google maps</a><a href="https://goo.gl/maps/9wk1Xm6rbmACkeMJ6" target="_blank" class="link-block-5 w-inline-block"><img src="images/google-maps.png" loading="lazy" width="80" alt="" class="image-12"></a></div></p>        </div>
      </div>
      <div class="col-sm" style="height: 600px;">
        <div id="map"> </div> 
      </div>
  </div>
</div>

  <div class="footer">
    <div class="container cc-footer">
      <div class="footer-column cc-footer"><a href="index.php" class="navigation-logo w-inline-block"><img src="images/all-x-all-2.png" width="143" srcset="images/all-x-all-2-p-500.png 500w, images/all-x-all-2-p-800.png 800w, images/all-x-all-2.png 1030w" sizes="(max-width: 479px) 30vw, 143px" alt=""></a>
        <div class="text-footer-credits">© 2020 All for All Inc, All rights reserved.</div>
      </div><div class="premium-intro"></div>
      <div class="footer-column">
        <div class="footer-links-list"><a href="index.php" class="link-footer">Inicio</a><a href="about.php" class="link-footer">Conócenos</a><a href="instituciones_info.php" aria-current="page" class="link-footer w--current">Instituciones</a><a href="intermediario_info.php" aria-current="page" class="link-footer w--current">Intermediarios</a></div>
        <div class="footer-social"><a href="#" class="link-social w-inline-block"><img src="images/icon-facebook.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-twitter.svg" alt=""></a><a href="#" class="link-social w-inline-block"><img src="images/icon-instagram.svg" alt=""></a></div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=5f571a518e3cdfe11baf4271" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->

   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-yZzrX2-m4td9UFKPTLaYwiW5TzD4vxc&callback=initMap" async defer></script>
   
        <script>
          var map;
          function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 14.521214, lng: -90.586320},
            zoom: 15,
            });
            var marker = new google.maps.Marker({            
            position: {lat: 14.520978, lng: -90.593513},          
            map: map,
            title: 'Asilo San José'
            });
            var marker1 = new google.maps.Marker({
            position: {lat: 14.513233, lng: -90.582939},
            map: map,
            title: 'Hogar de niños Liga de vida nueva'
            });
          }
        </script>
</body>
</html>

<?php
require('../../reportes-php/fpdf.php');
require('../../config/conf2.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('../../images/all-x-all-2.png',10,8,33);
    // Arial bold 15
    $this->SetFont('Arial','B',20);
    // Movernos a la derecha
    $this->Cell(70);
    // Título
    $this->Cell(50,20,'Suscriptores',0,0,'C');
    // Salto de línea
    $this->Ln(20);
    $this->SetLeftMargin(6);
    $this->SetFont('Arial','',12);
    $this->MultiCell(180,5,utf8_decode('Se visualizan todos los usuarios suscritos a la comunidad.
        '));
    $this->SetFont('Arial','B',12);
    $this->Cell(15, 10, 'ID', 1, 0, 'C', 0);
    $this->Cell(30, 10, 'Nombre', 1, 0, 'C', 0);
    $this->Cell(25, 10, 'Apellido', 1, 0, 'C', 0);
    $this->Cell(40, 10, 'Email', 1, 0, 'C', 0);
    $this->Cell(50, 10, utf8_decode('Tipo de Suscripción'), 1, 0, 'C', 0);
    $this->Cell(38, 10, 'Fecha de registro', 1, 1, 'C', 0);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
}
}

$consulta = "SELECT * FROM afa_suscriptor AFAS WHERE AFAS.Sus_EstadoData = 'Y'";
$res = $con->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->SetLeftMargin(6);

while ($row = $res->fetch_assoc()) {
	$pdf->Cell(15, 10, $row['Sus_Suscriptor'], 1, 0, 'C', 0);
	$pdf->Cell(30, 10, utf8_decode($row['Sus_Nombre']), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['Sus_Apellido']), 1, 0, 'C', 0);
    $pdf->Cell(40, 10, utf8_decode($row['Sus_Email']), 1, 0, 'C', 0);
    $pdf->Cell(50, 10, utf8_decode($row['Sus_TipoSuscripcion']), 1, 0, 'C', 0);
    $pdf->Cell(38, 10, utf8_decode($row['Sus_FechaRegistro']), 1, 1, 'C', 0);

}

$pdf->Output();
?>

<?php
require('../../reportes-php/fpdf.php');
require('../../config/conf2.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('../../images/all-x-all-2.png',10,8,33);
    // Arial bold 15
    $this->SetFont('Arial','B',20);
    // Movernos a la derecha
    $this->Cell(70);
    // Título
    $this->Cell(50,20,utf8_decode('Todos los víveres recibidos'),0,0,'C');
    // Salto de línea
    $this->Ln(20);
    $this->SetLeftMargin(6);
    $this->SetFont('Arial','',12);
    $this->MultiCell(180,5,utf8_decode('Se visualizan todas las especies tipo vívere recibidas en las donaciones.
        '));
    $this->SetFont('Arial','B',12);
    $this->Cell(20, 10, 'ID', 1, 0, 'C', 0);
    $this->Cell(50, 10, 'Cantidad (lb/unidad)', 1, 0, 'C', 0);
    $this->Cell(25, 10, 'Nombre', 1, 0, 'C', 0);
    $this->Cell(25, 10, 'Tipo', 1, 0, 'C', 0);
    $this->Cell(50, 10, 'Fecha de vencimiento', 1, 0, 'C', 0);
    $this->Cell(28, 10, utf8_decode('ID Donación'), 1, 1, 'C', 0);
    //$this->Cell(25, 10, 'Estado', 1, 1, 'C', 0);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
}
}

$consulta = "SELECT AFADetalle.DDR_DetalleDR, AFADetalle.DDR_Cantidad, AFAP.Pro_Nombre, AFATP.TP_TipoProducto, AFADetalle.DDR_FechaVencimiento, AFADetalle.Don_Donacion, AFAD.Don_Estado FROM afa_detalle_donacionrecibida AFADetalle
INNER JOIN afa_producto AFAP 
ON AFADetalle.DDR_Producto = AFAP.Pro_Producto
INNER JOIN afa_tipoproducto AFATP
ON AFAP.TP_idTipoProducto = AFATP.TP_idTipoProducto
INNER JOIN afa_donacion AFAD
ON AFAD.Don_Donacion = AFADetalle.Don_Donacion
WHERE AFAD.Don_Estado='Recibida' AND AFATP.TP_TipoProducto='Vívere';";
$res = $con->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->SetLeftMargin(6);

while ($row = $res->fetch_assoc()) {
	$pdf->Cell(20, 10, $row['DDR_DetalleDR'], 1, 0, 'C', 0);
	$pdf->Cell(50, 10, utf8_decode($row['DDR_Cantidad']), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['Pro_Nombre']), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['TP_TipoProducto']), 1, 0, 'C', 0);
    $pdf->Cell(50, 10, utf8_decode($row['DDR_FechaVencimiento']), 1, 0, 'C', 0);
    $pdf->Cell(28, 10, utf8_decode($row['Don_Donacion']), 1, 1, 'C', 0);
    //$pdf->Cell(25, 10, utf8_decode($row['Don_Estado']), 1, 1, 'C', 0);

}

$pdf->Output();
?>

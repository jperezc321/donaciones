<?php
require('../../reportes-php/fpdf.php');
require('../../config/conf2.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('../../images/all-x-all-2.png',10,8,33);
    // Arial bold 15
    $this->SetFont('Arial','B',20);
    // Movernos a la derecha
    $this->Cell(70);
    // Título
    $this->Cell(50,20,'Donaciones',0,0,'C');
    // Salto de línea
    $this->Ln(20);
    $this->SetLeftMargin(6);
    $this->SetFont('Arial','',12);
    $this->MultiCell(180,5,utf8_decode('Se visualizan todas las donaciones que se han recibido y entregado.
        '));
    $this->SetFont('Arial','B',8);
    $this->Cell(10, 10, 'ID', 1, 0, 'C', 0);
    $this->Cell(25, 10, 'Fecha recibido', 1, 0, 'C', 0);
    $this->Cell(25, 10, 'Fecha entregado', 1, 0, 'C', 0);
    $this->Cell(25, 10, utf8_decode('Usuario recibió'), 1, 0, 'C', 0);
    $this->Cell(25, 10, utf8_decode('Usuario entregó'), 1, 0, 'C', 0);
    $this->Cell(22, 10, utf8_decode('Intermediario'), 1, 0, 'C', 0);
    $this->Cell(22, 10, 'Donador', 1, 0, 'C', 0);
    $this->Cell(25, 10, utf8_decode('Familia recibió'), 1, 0, 'C', 0);
    $this->Cell(22, 10, utf8_decode('Estado'), 1, 1, 'C', 0);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
}
}

$consulta = "SELECT AFAD.Don_Donacion, AFAD.Don_FechaRecibido, AFAD.Don_FechaEntrega, AFAD.Us_UsuarioRecibe, AFAD.Us_UsuarioEntrega, AFAI.Int_Nombre,
            AFAS.Sus_Nombre, AFAS.Sus_Apellido, AFAF.Fam_Apellidos, AFAD.Don_Estado FROM afa_donacion AFAD 
            INNER JOIN afa_intermediario AFAI
            ON AFAI.Int_Intermediario = AFAD.Int_Intermediario
            INNER JOIN afa_suscriptor AFAS
            ON AFAS.Sus_Suscriptor = AFAD.Sus_Suscriptor
            INNER JOIN afa_familia AFAF
            ON AFAF.Fam_Familia = AFAD.Fam_Familia
            WHERE AFAD.Don_EstadoData = 'Y'
            ORDER BY AFAD.Don_Donacion";
$res = $con->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$pdf->SetLeftMargin(6);

while ($row = $res->fetch_assoc()) {
	$pdf->Cell(10, 10, $row['Don_Donacion'], 1, 0, 'C', 0);
	$pdf->Cell(25, 10, utf8_decode($row['Don_FechaRecibido']), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['Don_FechaEntrega']), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['Us_UsuarioRecibe']), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['Us_UsuarioEntrega']), 1, 0, 'C', 0);
    $pdf->Cell(22, 10, utf8_decode($row['Int_Nombre']), 1, 0, 'C', 0);
    $pdf->Cell(22, 10, utf8_decode($row['Sus_Nombre'].' '.$row['Sus_Apellido']), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['Fam_Apellidos']), 1, 0, 'C', 0);
    $pdf->Cell(22, 10, utf8_decode($row['Don_Estado']), 1, 1, 'C', 0);

}

$pdf->Output();
?>

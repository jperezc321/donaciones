<?php
require('../../reportes-php/fpdf.php');
require('../../config/conf2.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('../../images/all-x-all-2.png',10,8,33);
    // Arial bold 15
    $this->SetFont('Arial','B',20);
    // Movernos a la derecha
    $this->Cell(70);
    // Título
    $this->Cell(50,20,'Inventario',0,0,'C');
    // Salto de línea
    $this->Ln(20);
    $this->SetLeftMargin(9);
    $this->SetFont('Arial','',12);
    $this->MultiCell(180,5,utf8_decode('Se visualizan todas las especies donadas, asimismo sus características.
        '));
    $this->SetFont('Arial','B',12);
    $this->Cell(20, 10, 'ID', 1, 0, 'C', 0);
    $this->Cell(30, 10, 'Nombre', 1, 0, 'C', 0);
    $this->Cell(25, 10, 'Tipo', 1, 0, 'C', 0);
    $this->Cell(20, 10, 'Talla', 1, 0, 'C', 0);
    $this->Cell(25, 10, utf8_decode('Género'), 1, 0, 'C', 0);
    $this->Cell(25, 10, 'Edad', 1, 0, 'C', 0);
    $this->Cell(50, 10, 'Cantidad (lb/unidad)', 1, 1, 'C', 0);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
}
}

$consulta = "SELECT AFAP.Pro_Producto, AFAP.Pro_Nombre, AFATP.TP_TipoProducto, AFAP.Pro_EstadoData, 
            ATP.TPR_Nombre, AFAP.Pro_PrendaGenero, AFD.Ed_Nombre, SUM(AFDD.DDR_Cantidad) AS Inventario
            FROM afa_producto AFAP 
            INNER JOIN afa_tipoproducto AFATP ON AFAP.TP_idTipoProducto = AFATP.TP_idTipoProducto 
            LEFT JOIN afa_tallaprenda ATP ON ATP.TPR_idTallaPrenda = AFAP.Pro_PrendaTalla 
            LEFT JOIN afa_edad AFD ON AFD.Ed_Edad = AFAP.Pro_PrendaEdad 
            LEFT JOIN afa_detalle_donacionrecibida AFDD 
            ON AFDD.DDR_Producto = AFAP.Pro_Producto
            WHERE AFAP.Pro_EstadoData = 'Y'
            GROUP BY AFAP.Pro_Producto
            ORDER BY AFAP.Pro_Producto ASC";
$res = $con->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->SetLeftMargin(9);

while ($row = $res->fetch_assoc()) {
	$pdf->Cell(20, 10, $row['Pro_Producto'], 1, 0, 'C', 0);
	$pdf->Cell(30, 10, utf8_decode($row['Pro_Nombre']), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['TP_TipoProducto']), 1, 0, 'C', 0);
    $pdf->Cell(20, 10, utf8_decode($row['TPR_Nombre']?$row['TPR_Nombre']:"N/A"), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['Pro_PrendaGenero']?$row['Pro_PrendaGenero']:"N/A"), 1, 0, 'C', 0);
    $pdf->Cell(25, 10, utf8_decode($row['Ed_Nombre']?$row['Ed_Nombre']:"N/A"), 1, 0, 'C', 0);
    $pdf->Cell(50, 10, utf8_decode($row['Inventario']?$row['Inventario']:"0"), 1, 1, 'C', 0);

}

$pdf->Output();
?>
<?php
require('../../reportes-php/fpdf.php');
require('../../config/conf2.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    $this->Image('../../images/all-x-all-2.png',10,8,33);
    // Arial bold 15
    $this->SetFont('Arial','B',20);
    // Movernos a la derecha
    $this->Cell(70);
    // Título
    $this->Cell(50,20,'Instituciones',0,0,'C');
    // Salto de línea
    $this->Ln(20);
    $this->SetLeftMargin(8);
    $this->SetFont('Arial','',12);
    $this->MultiCell(180,5,utf8_decode('Se visualizan todos las instituciones que son parte de la comunidad.
        '));
    $this->SetFont('Arial','B',12);
    $this->Cell(15, 10, 'ID', 1, 0, 'C', 0);
    $this->Cell(50, 10, 'Nombre', 1, 0, 'C', 0);
    $this->Cell(60, 10, 'Email', 1, 0, 'C', 0);
    $this->Cell(40, 10, utf8_decode('Dirección'), 1, 0, 'C', 0);
    $this->Cell(30, 10, utf8_decode('No Teléfono'), 1, 1, 'C', 0);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
}
}

$consulta = "SELECT * FROM afa_institucion AFAIN WHERE AFAIN.Ins_EstadoData = 'Y'";
$res = $con->query($consulta);

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);
$pdf->SetLeftMargin(8);

while ($row = $res->fetch_assoc()) {
	$pdf->Cell(15, 10, $row['Ins_Institucion'], 1, 0, 'C', 0);
	$pdf->Cell(50, 10, utf8_decode($row['Ins_Nombre']), 1, 0, 'C', 0);
    $pdf->Cell(60, 10, utf8_decode($row['Ins_Email']), 1, 0, 'C', 0);
    $pdf->Cell(40, 10, utf8_decode($row['Ins_Direccion']), 1, 0, 'C', 0);
    $pdf->Cell(30, 10, utf8_decode($row['Ins_NoTelefono']), 1, 1, 'C', 0);
}

$pdf->Output();
?>
